
import os
#(pip install pillow para poder importar Image)
from PIL import Image
from flask import url_for, current_app

#Parámetros:
#selec_foto:foto seleccionada
#nvo_nombre:nombre con el que se va a guardar la foto en el servidor
#ruta_app:ruta en la que se guardará la foto en el servidor (app; ejemplo:/static/fotos_clases)
#ancho: ancho en pixeles con el que se guarda la foto
#alto: alto en pixeles con el que se guarda la foto

#Nota: Si ancho==0 and alto==0, la foto se guarda en su tamaño original.  
def agregar_foto(selec_foto,nvo_nombre,ruta_app,ancho,alto):
    nombre_archivo=selec_foto.filename
    ext_tipo=nombre_archivo.split('.')[-1]
    nombre_almacenamiento=str(nvo_nombre)+'.'+ext_tipo
    ruta=os.path.join(current_app.root_path,ruta_app,nombre_almacenamiento)

    if ancho==0 and alto==0:
        foto=Image.open(selec_foto)
    else:
        output_size=(ancho,alto)
        foto=Image.open(selec_foto)
        foto.thumbnail(output_size)

    ##output_size=(ancho,alto)
    #foto=Image.open(selec_foto)
    ##foto.thumbnail(output_size)
    foto.save(ruta)

    return nombre_almacenamiento
