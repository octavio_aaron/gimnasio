import os
from flask import Flask,flash
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import datetime as dt
from datetime import datetime, timedelta, date
import pytz
import sqlite3
from flask_login import LoginManager
from werkzeug.security import generate_password_hash,check_password_hash
from flask_login import UserMixin

login_manager=LoginManager()

# Configuración de la base de datos:
basedir=os.path.abspath(os.path.dirname(__file__))


app=Flask(__name__)
app.config['SECRET_KEY'] = 'perro'

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'data.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
#Conectamos la base de datos (db) con la aplicación (app) para realizar las migraciones (Migrate(app,db))
Migrate(app,db)

login_manager.init_app(app)
login_manager.login_view='login'


#################
#MODELOS:
#Tabla de Asistencia: relaciona a cada alumno con una clase y asigna la fecha en la que asiste el alumno
#Esta tabla se alimenta con las clases Alumnos y Clases.

class Asistencias(db.Model):
    __tablename__='asistencias'

    alumno_id=db.Column(db.Integer,db.ForeignKey('alumnos.id'),primary_key=True)
    alumno_nombre=db.Column(db.Text)
    clase_id=db.Column(db.Integer,db.ForeignKey('clases.id'),primary_key=True)
    clase_nombre=db.Column(db.Text)
    fecha=db.Column(db.DateTime,nullable=False, primary_key=True)

    #fecha=db.Column(db.DateTime,nullable=False,default=datetime.now(tz=pytz.UTC) - timedelta(hours=5),primary_key = True)

class Alumnos(db.Model):
    __tablename__='alumnos'
    id = db.Column(db.Integer,primary_key = True)
    nombre = db.Column(db.Text)
    sangre=db.Column(db.Text)
    gen=db.Column(db.Text)
    fec_nac=db.Column(db.Date)
    fec_insc=db.Column(db.Date)
    email=db.Column(db.String(64))
    cel=db.Column(db.String(15))
    contacto_emergencia=db.Column(db.Text)
    tel_emergencia=db.Column(db.String(15))
    observaciones=db.Column(db.Text)
    seguro=db.Column(db.String(10))


    asistencia=db.relationship('Asistencias', backref='alumno',lazy='dynamic')
    tarjeta=db.relationship('Tarjetas',backref='alumno',lazy='dynamic')


    def __init__(self,nombre,sangre,seguro,email,cel,contacto_emergencia,tel_emergencia,gen,observaciones,fec_nac):
        self.nombre = nombre
        self.email=email
        self.cel=cel
        self.sangre=sangre
        self.seguro=seguro
        self.contacto_emergencia=contacto_emergencia
        self.tel_emergencia=tel_emergencia
        self.gen=gen
        self.observaciones=observaciones
        fn=datetime.strptime(fec_nac,'%Y-%m-%d')
        self.fec_nac=dt.date(fn.year,fn.month,fn.day)
        #fi=datetime.strptime(fec_insc,'%Y-%m-%d')
        fi=(datetime.now(tz=pytz.UTC) - timedelta(hours=5))
        self.fec_insc=dt.date(fi.year,fi.month,fi.day)


    def __repr__(self):
        if self.gen=="Masculino":
            return f"{self.nombre} es alumno desde {self.fec_insc}"
        else:
            return f"{self.nombre} es alumna desde {self.fec_insc}"

class Clases(db.Model):
    __tablename__='clases'

    id = db.Column(db.Integer,primary_key = True)
    clase = db.Column(db.Text)
    tipo=db.Column(db.String(10))
    estatus=db.Column(db.String(10))
    horario=db.Column(db.Text)
#El precio es una variable de texto porque será usado en la página de la clase,
#no se hará ningún cálculo debido a que la estructura de precios puede variar en cada clase
    precio=db.Column(db.Text)
    descrip=db.Column(db.Text)
    profesor_id=db.Column(db.Integer,db.ForeignKey('profesores.id'))
    profesor_nombre=db.Column(db.Text)
    foto_clase=db.Column(db.String(120),nullable=False,default='foto_predeterminada.jpg')
    opinion=db.Column(db.Text)
    clase_permanente=db.Column(db.String(10))

    tarjeta=db.relationship('Tarjetas',backref='clase',lazy='dynamic')
    asistencia=db.relationship('Asistencias',backref='clase',lazy='dynamic')
    renta=db.relationship('Rentas',backref='clase',lazy='dynamic')


    def __init__(self,clase,profesor_id,tipo,descrip,precio,horario,opinion,clase_permanente,estatus='Activa',foto_clase='foto_predeterminada.jpg'):
        clase=clase.strip()
        self.clase = clase
        self.profesor_id=profesor_id
        self.tipo=tipo
        self.descrip=descrip
        self.precio=precio
        self.horario=horario
        self.estatus=estatus
        self.foto_clase=foto_clase
        self.opinion=opinion
        self.clase_permanente=clase_permanente

        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute ("SELECT nombre FROM profesores WHERE id={}".format(self.profesor_id))
        nom=cursor.fetchall()[0][0]
        self.profesor_nombre=nom

    def __repr__(self):
        return f"{self.clase}"

class Tarjetas(db.Model):
    __tablename__='tarjetas'

    id = db.Column(db.Integer,primary_key = True)
    alumno_id=db.Column(db.Integer,db.ForeignKey('alumnos.id'))
    alumno_nombre=db.Column(db.Text)
    clase_id=db.Column(db.Integer,db.ForeignKey('clases.id'))
    clase_nombre=db.Column(db.Text)
    n_clases=db.Column(db.Integer)
    precio_tarjeta=db.Column(db.Float)
    n_clases_disp=db.Column(db.Integer)
    fec_vencimiento=db.Column(db.Date)
    fec_compra=db.Column(db.Date)


    def __init__(self,alumno_id,clase_id,n_clases,precio_tarjeta,fec_vencimiento):
        hoy=(datetime.now(tz=pytz.UTC) - timedelta(hours=5))
        fec_compra=dt.date(hoy.year,hoy.month,hoy.day)

        #Poner en cero las clases disponibles de las tarjetas vencidas a la fecha de compra (hoy):
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute ("UPDATE tarjetas SET n_clases_disp=0 WHERE fec_vencimiento<'{}'".format(fec_compra))
        #db.session.commit()
        conexion.commit()

    #Antes de resgistrar la compra debe revisarse que la tarjeta sea única:
    #Que no haya una tarjeta con clases disponibles para el mismo alumno, para las misma clase y con vencimiento posterior a la feha de compra de la nueva tarjeta
        try:
            conexion=sqlite3.connect("data.sqlite")
            cursor=conexion.cursor()
            cursor.execute("SELECT fec_vencimiento FROM tarjetas WHERE alumno_id={} AND clase_id={} AND n_clases_disp>0 AND fec_vencimiento>'{}'".format(alumno_id,clase_id,fec_compra))
            self.t_vencimiento=cursor.fetchall()[0][0]
        except IndexError:
            #Si la consulta anterior está vacía significa que la compra PUEDE realizarse:
            self.alumno_id=alumno_id
            self.clase_id=clase_id
            self.n_clases = n_clases
            #Al comprar la tarjeta, todas las clases están disponibles, por lo que n_clases_disp=n_clases:
            self.n_clases_disp=n_clases
            self.precio_tarjeta=precio_tarjeta
            f=datetime.strptime(fec_vencimiento,'%Y-%m-%d')
            self.fec_vencimiento=dt.date(f.year,f.month,f.day)
            fc=(datetime.now(tz=pytz.UTC) - timedelta(hours=5))
            self.fec_compra=dt.date(fc.year,fc.month,fc.day)
            #Al construir la tarjeta se asigna el alumno_id y el clase_id, pero no los nombres;
            #por lo que se van a insertar con sqlite:
            #Conexión a la base de datos:


            #Nombre del alumno:
            cursor.execute("SELECT nombre FROM profesores WHERE (tipo='Alumno' OR tipo=='Profesor y alumno') AND id={}".format(self.alumno_id))
            alumno_nombre=cursor.fetchall()[0][0]
            self.alumno_nombre=alumno_nombre
            #Clase:
            cursor.execute('SELECT clase FROM clases WHERE id={}'.format(self.clase_id))
            clase_nombre=cursor.fetchall()[0][0]
            self.clase_nombre=clase_nombre
            self.tar_creada=True
        else:
            self.tar_creada=False

########


    def __repr__(self):
        if self.tar_creada:
            return f"Compra realizada.\n Usted adquirió {self.n_clases} sesiones. Recuerde que puede hacerlas válidas hasta el {self.fec_vencimiento}"
        else:
            return "Tu compra no pudo realizarse debido a que aún tienes un paquete de clases vigente.\n Aprovéchalo antes del {}".format(self.t_vencimiento)


########################################################################################
#Método para ampliar la vigencia de las tarjetas cuando:
#1) El maestro falta a clases
#2) Ajustes al calendario por días inhábiles
#3) Hay un error de cálculo en la vigencia de la tarjeta al momento de la compra
    #def actualiza_vigencia(self,delta_t):

#########################################################################################
class Profesores(db.Model):
    __tablename__='profesores'

    id = db.Column(db.Integer,primary_key = True)
    nombre = db.Column(db.Text)
    tipo=db.Column(db.String(16))
    sangre=db.Column(db.Text)
    gen=db.Column(db.Text)
    fec_nac=db.Column(db.Date)
    fec_insc=db.Column(db.Date)
    email=db.Column(db.String(64))
    cel=db.Column(db.String(16))
    contacto_emergencia=db.Column(db.Text)
    tel_emergencia=db.Column(db.String(16))
    observaciones=db.Column(db.Text)
    seguro=db.Column(db.String(10))
    cv=db.Column(db.Text)
    aviso_conf=db.Column(db.String(2))
    tutor=db.Column(db.Text)

    #asistencia=db.relationship('Asistencias',backref='profesor',lazy='dynamic')
    clase=db.relationship('Clases',backref='profesor',lazy='dynamic')
    renta=db.relationship('Rentas',backref='profesor',lazy='dynamic')


    def __init__(self,nombre,tipo,sangre,seguro,aviso_conf,email,cel,contacto_emergencia,tel_emergencia,gen,observaciones,cv,fec_nac,tutor):
        self.nombre = nombre
        self.tipo=tipo
        self.email=email
        self.cel=cel
        self.sangre=sangre
        self.seguro=seguro
        self.contacto_emergencia=contacto_emergencia
        self.tel_emergencia=tel_emergencia
        self.gen=gen
        self.observaciones=observaciones
        fn=datetime.strptime(fec_nac,'%Y-%m-%d')
        self.fec_nac=dt.date(fn.year,fn.month,fn.day)
        #fi=datetime.strptime(fec_insc,'%Y-%m-%d')
        fi=(datetime.now(tz=pytz.UTC) - timedelta(hours=5))
        self.fec_insc=dt.date(fi.year,fi.month,fi.day)
        self.cv=cv
        self.aviso_conf=aviso_conf
        self.tutor=tutor

    def __repr__(self):
        if self.gen=="Masculino":
            return f"{self.nombre} es profesor desde {self.fec_insc}"
        else:
            return f"{self.nombre} es profesora desde {self.fec_insc}"

class Rentas(db.Model):
    __tablename__='rentas'

    id = db.Column(db.Integer,primary_key = True)
    salon=db.Column(db.Text)
    total_cant_pagar=db.Column(db.Float)
    precio_salon=db.Column(db.Float)
    fec_renta=db.Column(db.Date)
    hora_inicial=db.Column(db.Time)
    hora_final=db.Column(db.Time)
    n_horas=db.Column(db.Float)
    profesor_id=db.Column(db.Integer,db.ForeignKey('profesores.id'))
    profesor_nombre=db.Column(db.Text)
    clase_id=db.Column(db.Integer,db.ForeignKey('clases.id'))
    clase_nombre=db.Column(db.Text)
    servicios=db.Column(db.Text)
    salon_cant_pagar=db.Column(db.Float)
    serv_cant_pagar=db.Column(db.Float)
    fec_actual=db.Column(db.Date)
    usuario=db.Column(db.String(100))
    estatus_reserva=db.Column(db.String(32))
    #iva=db.Column(db.Float)
    #total_cant_pagar=db.Column(db.Float)


    def __init__(self,profesor_id,salon,total_cant_pagar,precio_salon,fec_renta,n_horas,clase_id,servicios,salon_cant_pagar,serv_cant_pagar,usuario,estatus_reserva='Vigente',hora_inicial='00:00',hora_final='00:00'):
        # Se adjunta la cadena ":00.000000" a las horas para poder comparar el valor de la tabla Rentas con el de los parámetros del constructor.
        h_ini=hora_inicial+':00.000000'
        h_fin=hora_final+':00.000000'
        self.h0=hora_inicial
        self.h1=hora_final

    # Revisar que el salón no se haya rentado el mismo día a la misma hora:
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()

        try:
            cursor.execute("SELECT id FROM rentas WHERE estatus_reserva='Vigente' AND salon='{}' AND fec_renta='{}' AND ((hora_inicial<='{}' AND hora_final>'{}') OR (hora_inicial<'{}' AND hora_final>='{}') OR (hora_inicial>'{}' AND hora_final<'{}'))".format(salon,fec_renta,h_ini,h_ini,h_fin,h_fin,h_ini,h_fin))
            r=cursor.fetchall()[0][0]

        except IndexError:
            self.profesor_id=profesor_id
            self.salon=salon
            self.total_cant_pagar=total_cant_pagar
            self.precio_salon=precio_salon
            self.salon_cant_pagar=salon_cant_pagar
            self.serv_cant_pagar=serv_cant_pagar
            self.clase_id=clase_id
            self.servicios=servicios
            try:
                f=datetime.strptime(fec_renta,'%Y-%m-%d')
            except ValueError:
                self.renta=False
                x='Fecha incorrecta: El '+ fec_renta + ' no existe'
                m=['Rentar',False,x]
                flash(m)
            else:
                self.fec_renta=dt.date(f.year,f.month,f.day)
                hi=datetime.strptime(hora_inicial,'%H:%M')
                self.hora_inicial=dt.time(hi.hour,hi.minute)
                hf=datetime.strptime(hora_final,'%H:%M')
                self.hora_final=dt.time(hf.hour,hf.minute)
                self.n_horas=n_horas
                hoy=datetime.now(tz=pytz.UTC) - timedelta(hours=5)
                self.fec_actual=dt.date(hoy.year,hoy.month,hoy.day)
                self.usuario=usuario
                self.estatus_reserva=estatus_reserva

                cursor.execute('SELECT clase FROM clases WHERE id={}'.format(self.clase_id))
                clase_nombre=cursor.fetchall()[0][0]
                self.clase_nombre=clase_nombre
                cursor.execute('SELECT nombre FROM profesores WHERE id={}'.format(self.profesor_id))
                profesor_nombre=cursor.fetchall()[0][0]
                self.profesor_nombre=profesor_nombre
                self.renta=True

        else:
            self.salon=salon
            f=datetime.strptime(fec_renta,'%Y-%m-%d')
            self.fec_renta=dt.date(f.year,f.month,f.day)
            self.renta=False


    def __repr__(self):
        if self.renta:
            return "{} rentó el salón {} para el día {} de {} a {} hrs.".format(self.profesor_nombre,self.salon,self.fec_renta,self.h0,self.h1)
        else:
            #return "Lo sentimos, el salón {} ya se encuentra apartado para el día {} durante el horario que deseas".format(self.salon,self.fec_renta)
            return "La renta no pudo realizarse. El salón {} no está disponible para el día {} en el horario que indicas.".format(self.salon,self.fec_renta)


class Servicios(db.Model):
    __tablename__='servicios'

    id = db.Column(db.Integer,primary_key = True)
    nombre=db.Column(db.Text)
    descrip=db.Column(db.Text)
    precio=db.Column(db.Float)


    materiales=db.relationship('Serv_componentes',backref='servicio',lazy='dynamic')

    def __init__(self,nombre,descrip,precio):
        self.nombre=nombre
        self.descrip=descrip
        self.precio=precio


    def __repr__(self):
        return "{}".format(self.nombre)


class Materiales(db.Model):
    __tablename__='materiales'

    id=db.Column(db.Integer,primary_key=True)
    nombre=db.Column(db.Text)
    n_piezas=db.Column(db.Integer)
    fec_mantenimiento=db.Column(db.Date)
    estado=db.Column(db.String(16))
    observaciones=db.Column(db.Text)

    servicios=db.relationship('Serv_componentes',backref='material',lazy='dynamic')

    def __init__(self,nombre,n_piezas,fec_mantenimiento,estado,observaciones):

        if fec_mantenimiento=='':
            fm=(datetime.now(tz=pytz.UTC) - timedelta(hours=5))
            self.fec_mantenimiento=dt.date(fm.year,fm.month,fm.day)
        else:
            try:
                fm=datetime.strptime(fec_mantenimiento,'%Y-%m-%d')

            except ValueError:
                x='El formato de la fecha debe ser AAAA-MM-DD; por ejemplo, 2016-08-24'
                mensaje=[False,x]
                flash(mensaje)
            else:
                self.fec_mantenimiento=dt.date(fm.year,fm.month,fm.day)
                self.nombre=nombre
                self.n_piezas=n_piezas
                self.estado=estado
                self.observaciones=observaciones
                

    def __repr__(self):
        return "{}".format(self.nombre)

class Serv_componentes(db.Model):
    __tablename__='serv_componentes'

    servicio_id=db.Column(db.Integer,db.ForeignKey('servicios.id'),primary_key=True)
    servicio_nombre=db.Column(db.Text)
    material_id=db.Column(db.Integer,db.ForeignKey('materiales.id'),primary_key=True)
    material_nombre=db.Column(db.Text)
    n_piezas=db.Column(db.Integer)

    def __init__(self,servicio_id,material_id):
        self.servicio_id=servicio_id
        self.material_id=material_id

        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute('SELECT nombre FROM servicios WHERE id={}'.format(self.servicio_id))
        servicio_nombre=cursor.fetchall()[0][0]
        self.servicio_nombre=servicio_nombre

        cursor.execute("SELECT nombre,n_piezas FROM materiales WHERE id={}".format(self.material_id))
        r=cursor.fetchall()
        material_nombre=r[0][0]
        n_piezas=r[0][1]
        self.material_nombre=material_nombre
        self.n_piezas=n_piezas


    def agregar_material(self):
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        try:
            cursor.execute("INSERT INTO serv_componentes VALUES ('{}','{}','{}','{}','{}')".format(self.servicio_id,self.servicio_nombre,self.material_id,self.material_nombre,self.n_piezas))
            conexion.commit()
        except sqlite3.IntegrityError:
            pass

    def retirar_material(self):
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute("DELETE FROM serv_componentes WHERE servicio_id={} AND material_id={}".format(self.servicio_id,self.material_id))
        conexion.commit()


    def __repr__(self):
        return "Se asignó el material ({}) al paquete de servicios {}".format(self.material_nombre,self.servicio_nombre)


@login_manager.user_loader
def load_user(usuario_id):
    return Usuarios.query.get(usuario_id)

class Usuarios(db.Model,UserMixin):

    __tablename__='usuarios'

    id=db.Column(db.Integer,primary_key=True)
    email=db.Column(db.String(64),unique=True,index=True)
    usuario=db.Column(db.String(64),unique=True,index=True)
    password_hash=db.Column(db.String(128))
    perfil=db.Column(db.String(64))

    def __init__(self,email,usuario,password,perfil='Alumno'):
        self.email=email
        self.usuario=usuario
        self.password_hash=generate_password_hash(password)
        self.perfil=perfil

    #Método para revisar contraseñas:

    def checar_password(self,password):
        return check_password_hash(self.password_hash,password)


class Conceptos(db.Model):
    __tablename__='conceptos'

    id=db.Column(db.Integer,primary_key=True)
    concepto=db.Column(db.String(64))

    def __init__(self,concepto):
        self.concepto=concepto

    def __repr__(self):
        return "{} fue creado correctamente".format(self.concepto)


class Movimientos(db.Model):

    __tablename__='movimientos'

    id=db.Column(db.Integer,primary_key=True)
    nombre=db.Column(db.String(90))
    tipo=db.Column(db.String(10))
    usuario=db.Column(db.String(90))
    mov=db.Column(db.String(10))
    monto=db.Column(db.Float)
    fec_mov=db.Column(db.Date)
    concepto=db.Column(db.Text)
    descrip=db.Column(db.Text)
    folio=db.Column(db.Integer)


    def __init__(self,nombre,tipo,usuario,mov,monto,concepto,descrip,folio):
        self.nombre=nombre
        self.tipo=tipo
        self.usuario=usuario
        self.mov=mov
        self.monto=monto
        self.concepto=concepto
        self.descrip=descrip
        fi=(datetime.now(tz=pytz.UTC) - timedelta(hours=5))
        self.fec_mov=dt.date(fi.year,fi.month,fi.day)
        self.folio=folio

    def __repr__(self):
        return "Se registró una {} de {} pesos por concepto de {}".format(self.mov,self.monto,self.concepto)

class Precio_clases(db.Model):

    __tablename__='precio_clases'

    #id=db.Column(db.Integer,primary_key=True)
    clase_id=db.Column(db.String(90),primary_key=True)
    clase_nombre=db.Column(db.String(90))
    precio=db.Column(db.Float,primary_key=True)
    cantidad=db.Column(db.Integer,primary_key=True)
    vigencia=db.Column(db.Integer)

#amir avalos salazar es un niño muy inteligente
    def __init__(self,clase_id,precio,cantidad,vigencia):
        self.clase_id=clase_id
        self.precio=precio
        self.cantidad=cantidad
        self.vigencia=vigencia


        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute('SELECT clase FROM clases WHERE id={}'.format(self.clase_id))
        clase_nombre=cursor.fetchall()[0][0]
        self.clase_nombre=clase_nombre

    def agregar_precio(self):
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute("INSERT INTO precio_clases VALUES ('{}','{}','{}','{}','{}')".format(self.clase_id,self.clase_nombre,self.precio,self.cantidad,self.vigencia))
        conexion.commit()

    def retirar_precio(self):
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute("DELETE FROM precio_clases WHERE clase_id={} AND precio={} AND cantidad={}".format(self.clase_id,self.precio,self.cantidad))
        conexion.commit()

    def __repr__(self):
        return "Se registró un par precio y cantidad para la clase {}".format(self.clase)

class Reportes_generales(db.Model):

    __tablename__='reportes_generales'

    id=db.Column(db.Integer,primary_key=True)
    reporte=db.Column(db.String(90))
    encabezados=db.Column(db.String)
    tabla=db.Column(db.String(90))

    def __init__(self,reporte,encabezados,tabla):
        self.reporte=reporte
        self.encabezados=encabezados
        self.tabla=tabla

    def __repr__(self):
        return "Reporte creado: {}".format(self.reporte)

class Config_email_inst(db.Model):

    __tablename__='config_email_inst'

    id=db.Column(db.Integer,primary_key=True)
    #nombre=db.Column(db.String)
    email=db.Column(db.String(64))
    password=db.Column(db.String)

    def __init__(self,email,password):
        self.email=email
        self.password=password
