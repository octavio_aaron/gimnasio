from modelos import *
import sqlite3
import datetime as dt
from datetime import datetime, timedelta
import pytz
from forms import *
from flask import Flask,render_template,url_for,redirect,flash,abort,request,stream_with_context
from flask_login import login_user,login_required,logout_user,current_user
from autoemail import *
import csv
from werkzeug.datastructures import Headers
from werkzeug.wrappers import Response
from io import StringIO
from admin_fotos import *
import pandas as pd
import numpy as np


#CREANDO TABLAS:
db.create_all()
#Tomando asistencia:

class Pase_lista ():

    def __init__(self,alumno_id,clase_id,fecha=(datetime.now(tz=pytz.UTC) - timedelta(hours=5))):
        self.alumno_id=alumno_id
        self.clase_id=clase_id
        self.fecha=dt.date(fecha.year,fecha.month,fecha.day)
        #Al construir la tarjeta se asigna el alumno_id y el clase_id, pero no los nombres;
        #por lo que se van a insertar con sqlite:
        #Conexión a la base de datos:
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()

        #Nombre del alumno:
        cursor.execute("SELECT nombre FROM profesores WHERE (tipo='Alumno' OR tipo=='Profesor y alumno') AND id={}".format(self.alumno_id))
        alumno_nombre=cursor.fetchall()[0][0]
        self.alumno_nombre=alumno_nombre
        #Clase:
        cursor.execute('SELECT clase FROM clases WHERE id={}'.format(self.clase_id))
        clase_nombre=cursor.fetchall()[0][0]
        self.clase_nombre=clase_nombre


        #Vigencia de las tarjetas:
        #Si la tarjeta ya prescribió, las clases disponibles se cancelarán debido a que el alumno no las usó:
        cursor.execute('SELECT fec_vencimiento,id FROM tarjetas WHERE alumno_id={} AND clase_id={} AND n_clases_disp>0'.format(self.alumno_id,self.clase_id))
        q=cursor.fetchall()
        try:
            fec_vencimiento=datetime.strptime(q[0][0],'%Y-%m-%d')
        except IndexError:
            self.asis=2
        else:
            fec_vencimiento=dt.date(fec_vencimiento.year,fec_vencimiento.month,fec_vencimiento.day)
            self.venc=fec_vencimiento
            id_tar=q[0][1]
            if self.fecha > fec_vencimiento:
                self.asis=1
                print('La tarjeta ya venció')
                conexion=sqlite3.connect("data.sqlite")
                cursor=conexion.cursor()
                cursor.execute("UPDATE tarjetas SET n_clases_disp=0 WHERE alumno_id={} AND clase_id={} AND id={}".format(self.alumno_id, self.clase_id, id_tar))
            else:
                print('La tarjeta no ha vencido')

        #Consulta de las clases disponible en la tarjeta: Si la tarjeta tiene clases disponibles, registra la asistencia;
        #en caso contrario avisa la imposibilidad de tomar clases
        #cursor.execute('SELECT n_clases_disp FROM tarjetas WHERE alumno_id={} AND clase_id={} AND n_clases_disp>0'.format(self.alumno_id,self.clase_id))
            try:
                cursor.execute('SELECT n_clases_disp FROM tarjetas WHERE alumno_id={} AND clase_id={} AND n_clases_disp>0'.format(self.alumno_id,self.clase_id))
                n_clases_disp=cursor.fetchall()[0][0]
                n_clases_disp_act=n_clases_disp-1
            except IndexError:
                if self.asis!=1:
                    self.asis=2
                print ("Lo sentimos, debes adquirir tus clases para poder asistir a {}.\nConsulta nuestras promociones".format(self.clase_nombre))
            else:
                #Actualización de las clases disponibles de la tarjeta:
                cursor.execute("UPDATE tarjetas SET n_clases_disp={}, alumno_nombre='{}', clase_nombre='{}' WHERE alumno_id={} AND clase_id={} AND n_clases_disp>0".format(n_clases_disp_act, self.alumno_nombre, self.clase_nombre, self.alumno_id, self.clase_id))
                try:
                    #Registro en la lista de asistencia:
                    cursor.execute("INSERT INTO asistencias VALUES ('{}','{}','{}','{}','{}')".format(self.alumno_id,self.alumno_nombre,self.clase_id,self.clase_nombre,self.fecha))
                except sqlite3.IntegrityError:
                    self.asis=3
                else:
                    self.asis=4

                print (f"La asistencia de {self.alumno_nombre} a la clase {self.clase_nombre} ya fue registrada")

                conexion.commit()
                conexion.close()

    def __repr__(self):
        if self.asis==1:
            return "Lo sentimos, tu paquete de clases expiró el {}. \n Debes adquirir tus clases para poder asistir a {}.\n Consulta nuestras promociones".format(self.venc,self.clase_nombre)
        elif self.asis==2:
            return "Lo sentimos, debes adquirir tus clases para poder asistir a {}.\n Consulta nuestras promociones".format(self.clase_nombre)
        elif self.asis==3:
            return "Lo sentimos, ya se había registrado la asistencia de {} a la clase de {} del día {}".format(self.alumno_nombre,self.clase_nombre,self.fecha)
        elif self.asis==4:
            return "Se registró la asistencia de {} a la clase de {} del día {}".format(self.alumno_nombre,self.clase_nombre,self.fecha)

#####
#VISTAS:

@app.route('/')
def inicio():
    return render_template('inicio.html')

@app.route('/quienes_somos')
def quienes_somos():
    liga="https://scontent.fmex7-1.fna.fbcdn.net/v/t1.0-9/15622245_1281695845210497_149629578480007475_n.jpg?_nc_cat=101&_nc_pt=1&_nc_ht=scontent.fmex7-1.fna&oh=04cde45532175a9765153d9018887f02&oe=5CB1EB31"


    return render_template('quienes_somos.html',liga=liga)

@app.route('/bienvenida')
#@login_required:Decorador que condiciona el acceso a la vista al ingreso de contraseña
@login_required
def bienvenida_usuario():
    return render_template('bienvenida_usuario.html')

@app.route('/salir')
@login_required
def salir():
    logout_user()
    #x='Saliste del sistema.'
    #mensaje=[True,x]

    #flash(mensaje)
    return redirect(url_for('inicio'))

@app.route('/entrar',methods=['GET','POST'])
def entrar():

    entrar=Login_forma()
    if entrar.validate_on_submit():
        usuario=Usuarios.query.filter_by(email=entrar.email.data).first()

        if usuario is None:
            x=[False,'Este usuario no se encuentra registrado. Revise su información o regístrese.']
            flash(x)
        else:
            if usuario.checar_password(entrar.password.data) and usuario is not None:
                login_user(usuario)
                next=request.args.get('next')

                if next==None or not next[0]=='/':
                    next=url_for('bienvenida_usuario')


                return redirect(next)

            else:
                x=[False,'La contraseña es incorrecta.']
                flash(x)

    return render_template('ingresar.html',entrar=entrar)

@app.route('/cambiar_password',methods=['GET','POST'])
def cambiar_password():
    nvo=Cambiar_password_forma()

    if nvo.validate_on_submit():
        try:
            nvo.revisar_email(nvo.email.data)
        except ValidationError:
            x='Este correo no se ha registrado. Favor de revisarlo.'
            mensaje=[False,x]
            flash(mensaje)

        else:

            conexion=sqlite3.connect("data.sqlite")
            cursor=conexion.cursor()
            cursor.execute("SELECT id,usuario,password_hash FROM usuarios WHERE email='{}'".format(nvo.email.data))
            r=cursor.fetchall()
            id=r[0][0]
            usuario=r[0][1]
            password_hash=r[0][2]
            #Si la contraseña proporcionada antes coincide con la almacenada en la BD, entonces...
            if not check_password_hash(password_hash,nvo.ant_password.data):
                x='La contraseña proporcionada no es corecta'
                mensaje=[False,x]
                flash(mensaje)

            else:
                password_hash=generate_password_hash(nvo.nvo_password.data)
                conexion=sqlite3.connect("data.sqlite")
                cursor=conexion.cursor()
                cursor.execute ("UPDATE usuarios SET password_hash='{}' WHERE id={}".format(password_hash,id))
                conexion.commit()
                conexion.close()
                x='Tu contraseña ha sido cambiada'
                mensaje=[True,x]
                flash(mensaje)
                return redirect(url_for('entrar'))

    return render_template('cambiar_password.html',nvo=nvo)

@app.route('/recuperar_password',methods=['GET','POST'])
def recuperar_password():

    recu=Recuperar_password_forma()

    if recu.validate_on_submit():
        try:
            recu.revisar_email(recu.email.data)
        except ValidationError:
            x='Este correo no se ha registrado. Favor de revisarlo.'
            mensaje=[False,x]
            flash(mensaje)

        else:
            conexion=sqlite3.connect("data.sqlite")
            cursor=conexion.cursor()
            cursor.execute("SELECT id,usuario,password_hash FROM usuarios WHERE email='{}'".format(recu.email.data))
            r=cursor.fetchall()
            id=r[0][0]
            usuario=r[0][1]
            #Para simplificar, asignamos como contraseña temporal los últimos 8 dígitos del "password_hash" alojado en BD
            temp_password=r[0][2][-8:]
            #Reemplazamos la contraseña anterior con la contraseña temporal:
            password_hash=generate_password_hash(temp_password)
            cursor.execute ("UPDATE usuarios SET password_hash='{}' WHERE id={}".format(password_hash,id))
            conexion.commit()

            cursor.execute("SELECT * FROM config_email_inst WHERE id=1")
            r=cursor.fetchall()
            remitente=r[0][1]
            password=r[0][2]

            conexion.close()

            destinatario=recu.email.data
            mensaje='Tu contraseña temporal es: '+temp_password+'. Te recomendamos cambiarla.'
            titulo='Restitución de contraseña de Armonía en Movimiento'

            correo=Auto_email()
            try:
                correo.enviar_correo(destinatario,remitente,password,mensaje,titulo)
            except ValidationError:
                x='La contraseña no pudo enviarse. Revisa tu conexión a internet'
                mensaje=[False,x]
                flash(mensaje)
            else:
                x='La contraseña ha sido enviada al correo con el que te registraste. Te recomendamos cambiarla'
                mensaje=[True,x]
                flash(mensaje)

    return render_template('recuperar_password.html',recu=recu)


@app.route('/registro',methods=['GET','POST'])
def registro():
    registro=Registro_forma()

    if registro.validate_on_submit():

        try:
            registro.checar_email(registro.email.data)
        except ValidationError:
            x='Este correo ya fue registrado. Proporciona otro o recupera tu contraseña.'
            mensaje=[False,x]
            flash(mensaje)

        else:
            try:
                registro.checar_usuario(registro.usuario.data)
            except ValidationError:
                x='Este usuario ya fue registrado. Proporciona otro o recupera tu contraseña.'
                mensaje=[False,x]
                flash(mensaje)

            else:
                usuario=Usuarios(email=registro.email.data,usuario=registro.usuario.data,password=registro.password.data)
                db.session.add(usuario)
                db.session.commit()
                #Por construcción el primer usuario registrado será directivo; es decir, un administrador del sistema
                if usuario.id==1:
                    usuario.perfil='Directivo'
                    db.session.commit()

                x='Gracias por registrarte. Ahora puedes ingresar.'
                mensaje=[True,x]
                flash(mensaje)
                return redirect(url_for('entrar'))

    return render_template('registro.html',registro=registro)


@app.route('/agregar_profesor',methods=['GET','POST'])
@login_required

def agregar_profesor():
    def anio_nac(anio_ini,n_anio):
        l_anios=[]
        a=0
        while a<n_anio:
            x=(str(anio_ini),str(anio_ini))
            l_anios.append(x)
            a=a+1
            anio_ini=anio_ini+1
        return l_anios

    lista_anios=anio_nac(1950,80)


    inscripcion=Agregar_profesor_forma()
    accion=inscripcion.accion.data
    mes_nac=inscripcion.mes_nac.data
    dia_nac=inscripcion.dia_nac.data
    inscripcion.anio_nac.choices=lista_anios
    anio_nac=inscripcion.anio_nac.data


    if accion=='Modificar':
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute('SELECT id,nombre FROM Profesores')
        lista_profesores=cursor.fetchall()
        #IMPORTANTE: convertir a cadena el identificador de la clase.
        #De lo contrario no funcionará la forma de selección, SelectField
        lista_profesores=[(str(id),nombre) for id,nombre in lista_profesores]
        inscripcion.profesor_mod.choices=lista_profesores
        profesor_mod=inscripcion.profesor_mod.data


    if inscripcion.validate_on_submit():
        nombre=inscripcion.nombre.data
        tipo=inscripcion.tipo.data
        email=inscripcion.email.data
        cel=inscripcion.cel.data
        contacto_emergencia=inscripcion.contacto_emergencia.data
        tel_emergencia=inscripcion.tel_emergencia.data
        gen=inscripcion.gen.data
        observaciones=inscripcion.observaciones.data
        cv=inscripcion.cv.data
        #fec_nac=inscripcion.fec_nac.data
        fec_nac=anio_nac+'-'+mes_nac+'-'+dia_nac
        #fec_insc=inscripcion.fec_insc.data
        sangre=inscripcion.sangre.data
        seguro=inscripcion.seguro.data
        aviso_conf=inscripcion.aviso_conf.data
        tutor=inscripcion.tutor.data
        menor=inscripcion.menor.data


        try:
            inscripcion.checar_profesor(inscripcion.nombre.data)
        except ValidationError:
            x='Este profesor ya fue registrado anteriormente.'
            mensaje=[False,x]
            flash(mensaje)

        else:
            try:
                inscripcion.checar_email(inscripcion.email.data)
            except ValidationError:
                x='Este correo eletrónico ya fue registrado anteriormente.'
                mensaje=[False,x]
                flash(mensaje)

            else:
                if accion=='Agregar':
                    m=True
                    if nombre=='':
                        x='Favor de indicar el nombre completo de la persona a la que deseas inscribir'
                        m1=[False,x]
                        flash(m1)
                        m=False
                    elif tipo=='':
                        x='Favor de indicar si la persona es profesor o alumno'
                        m2=[False,x]
                        flash(m2)
                        m=False
                    elif sangre=='':
                        x='Favor de indicar el tipo de sangre'
                        m3=[False,x]
                        flash(m3)
                        m=False
                    elif seguro=='':
                        x='Favor de indicar si la persona cubrió su cooperación para el seguro'
                        m4=[False,x]
                        flash(m4)
                        m=False
                    elif email=='':
                        x='Favor de indicar un correo electrónico'
                        m5=[False,x]
                        flash(m5)
                        m=False
                    elif contacto_emergencia=='':
                        x='Favor de indicar un contacto para emergencias'
                        m6=[False,x]
                        flash(m6)
                        m=False
                    elif tel_emergencia=='':
                        x='Favor de indicar un teléfono para emergencias'
                        m7=[False,x]
                        flash(m7)
                        m=False

                    elif menor=='Menor' and tutor=='':
                        x='Favor de indicar el nombre del tutor'
                        m8=[False,x]
                        flash(m8)
                        m=False

                    if m==True:
                        try:
                            nuevo_profesor=Profesores(nombre,tipo,sangre,seguro,aviso_conf,email,cel,contacto_emergencia,tel_emergencia,gen,observaciones,cv,fec_nac,tutor)
                            db.session.add(nuevo_profesor)
                            db.session.commit()
                            db.session.close
                            x='Se registró a '+nuevo_profesor.nombre
                            mensaje=[True,x]
                            flash(mensaje)
                        except ValueError:
                            if fec_nac=='':
                                x='Favor de indicar la fecha de nacimiento'
                                mensaje=[False,x]
                                flash(mensaje)

                elif accion=='Modificar':
                    profesor_mod=int(profesor_mod)

                    conexion=sqlite3.connect("data.sqlite")
                    cursor=conexion.cursor()
                    cursor.execute("SELECT * FROM profesores WHERE id={}".format(profesor_mod))
                    r=cursor.fetchall()


                    if nombre=="":
                        nombre=r[0][1]

                    if tipo=="":
                        tipo=r[0][2]

                    if sangre=="Pendiente":
                        sangre=r[0][3]

                    if gen=="":
                        gen=r[0][4]

                    if fec_nac=="1950-01-01":
                        fec_nac=r[0][5]

                    if email=="":
                        email=r[0][7]

                    if cel=="":
                        cel=r[0][8]

                    if contacto_emergencia=="":
                        contacto_emergencia=r[0][9]

                    if tel_emergencia=="":
                        tel_emergencia=r[0][10]

                    if observaciones=="":
                        observaciones=r[0][11]

                    if seguro=="":
                        seguro=r[0][12]

                    if cv=="":
                        cv=r[0][13]

                    if aviso_conf=="":
                        aviso_conf==r[0][14]


                    conexion=sqlite3.connect("data.sqlite")
                    cursor=conexion.cursor()
                    cursor.execute("UPDATE profesores SET nombre='{}',tipo='{}',sangre='{}', seguro='{}', email='{}', cel='{}',contacto_emergencia='{}', tel_emergencia='{}', gen='{}',observaciones='{}', fec_nac='{}', cv='{}', aviso_conf='{}' WHERE id={}".format(nombre,tipo,sangre,seguro,email,cel,contacto_emergencia,tel_emergencia,gen,observaciones,fec_nac,cv,aviso_conf,profesor_mod))
                    conexion.commit()
                    conexion.close()
                    x='La información del profesor fue modificada'
                    mensaje=[True,x]
                    flash(mensaje)

                else:
                    #Dejo este else para incluír la opción de eliminar en caso de ser necesario
                    pass


    if current_user.perfil=='Alumno':
        return redirect(url_for('acceso_denegado'))
    else:
        return render_template('agregar_profesor.html', inscripcion=inscripcion)


@app.route('/acceso_denegado')
@login_required

def acceso_denegado():
    return render_template('acceso_denegado.html')


@app.route('/lista_profesores')
@login_required

def lista_profesores():
    profesores=Profesores.query.all()
    return render_template('lista_profesores.html',profesores=profesores)


@app.route('/agregar_alumno',methods=['GET','POST'])
@login_required

def agregar_alumno():
    inscripcion=Agregar_alumno_forma()
    accion=inscripcion.accion.data


    if accion=='Modificar':
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute('SELECT id,nombre FROM Alumnos')
        lista_alumnos=cursor.fetchall()
        #IMPORTANTE: convertir a cadena el identificador de la clase.
        #De lo contrario no funcionará la forma de selección, SelectField
        lista_alumnos=[(str(id),nombre) for id,nombre in lista_alumnos]
        inscripcion.alumno_mod.choices=lista_alumnos
        alumno_mod=inscripcion.alumno_mod.data



    if inscripcion.validate_on_submit():
        nombre=inscripcion.nombre.data
        email=inscripcion.email.data
        cel=inscripcion.cel.data
        contacto_emergencia=inscripcion.contacto_emergencia.data
        tel_emergencia=inscripcion.tel_emergencia.data
        gen=inscripcion.gen.data
        observaciones=inscripcion.observaciones.data
        fec_nac=inscripcion.fec_nac.data
        #fec_insc=inscripcion.fec_insc.data
        sangre=inscripcion.sangre.data
        seguro=inscripcion.seguro.data

        try:
            inscripcion.checar_alumno(inscripcion.nombre.data)
        except ValidationError:
            x='Este alumno ya fue registrado anteriormente.'
            mensaje=[False,x]
            flash(mensaje)

        else:
            try:
                inscripcion.checar_email(inscripcion.email.data)
            except ValidationError:
                x='Este correo electrónico ya fue registrado anteriormente.'
                mensaje=[False,x]
                flash(mensaje)

            else:
                if accion=='Agregar':
                    nuevo_alumno=Alumnos(nombre,sangre,seguro,email,cel,contacto_emergencia,tel_emergencia,gen,observaciones,fec_nac)
                    db.session.add(nuevo_alumno)
                    db.session.commit()
                    db.session.close

                    x='Se registró a '+nuevo_alumno.nombre
                    mensaje=[True,x]
                    flash(mensaje)

                elif accion=='Modificar':
                    alumno_mod=int(alumno_mod)

                    conexion=sqlite3.connect("data.sqlite")
                    cursor=conexion.cursor()
                    cursor.execute("SELECT * FROM alumnos WHERE id={}".format(alumno_mod))
                    r=cursor.fetchall()


                    if nombre=="":
                        nombre=r[0][1]

                    if sangre=="":
                        sangre=r[0][2]

                    if gen=="":
                        gen=r[0][3]

                    if fec_nac=="":
                        fec_nac=r[0][4]

                    if email=="":
                        email=r[0][6]

                    if cel=="":
                        cel=r[0][7]

                    if contacto_emergencia=="":
                        contacto_emergencia=r[0][8]

                    if tel_emergencia=="":
                        tel_emergencia=r[0][9]

                    if observaciones=="":
                        observaciones=r[0][10]

                    if seguro=="":
                        seguro=r[0][11]

                    conexion=sqlite3.connect("data.sqlite")
                    cursor=conexion.cursor()
                    cursor.execute("UPDATE alumnos SET nombre='{}',sangre='{}', seguro='{}', email='{}', cel='{}',contacto_emergencia='{}', tel_emergencia='{}', gen='{}',observaciones='{}', fec_nac='{}' WHERE id={}".format(nombre,sangre,seguro,email,cel,contacto_emergencia,tel_emergencia,gen,observaciones,fec_nac,alumno_mod))
                    conexion.commit()
                    conexion.close()
                    x='La información del alumno fue modificada'
                    mensaje=[True,x]
                    flash(mensaje)

                else:
                    #Dejo este else para incluír la opción de eliminar en caso de ser necesario
                    pass

        #return redirect(url_for('lista_alumnos'))

    if current_user.perfil=='Alumno':
        return redirect(url_for('acceso_denegado'))
    else:
        return render_template('agregar_alumno.html', inscripcion=inscripcion)

@app.route('/lista_alumnos')
@login_required

def lista_alumnos():
    alumnos=Alumnos.query.all()
    return render_template('lista_alumnos.html',alumnos=alumnos)

@app.route('/agregar_clase',methods=['GET','POST'])
@login_required

def agregar_clase():
    conexion=sqlite3.connect("data.sqlite")
    cursor=conexion.cursor()
    cursor.execute("SELECT id,nombre FROM Profesores WHERE tipo='Profesor' OR tipo=='Profesor y alumno' " )
    lista_prof_disp=cursor.fetchall()


    oferta_clases=Agregar_clase_forma()
    oferta_clases.profesor_id.choices=lista_prof_disp
    accion=oferta_clases.accion.data

    if accion=='Modificar':
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute('SELECT id,clase FROM Clases')
        lista_clases=cursor.fetchall()
        #IMPORTANTE: convertir a cadena el identificador de la clase.
        #De lo contrario no funcionará la forma de selección, SelectField
        lista_clases=[(str(id),clase) for id,clase in lista_clases]
        oferta_clases.clase_mod.choices=lista_clases
        clase_mod=oferta_clases.clase_mod.data

    nombre=oferta_clases.nombre.data
    tipo=oferta_clases.tipo.data
    descrip=oferta_clases.descrip.data
    profesor_id=oferta_clases.profesor_id.data
    horario=oferta_clases.horario.data
    precio=oferta_clases.precio.data
    estatus=oferta_clases.estatus.data
    opinion=oferta_clases.opinion.data
    clase_permanente=oferta_clases.clase_permanente.data

    if oferta_clases.validate_on_submit():

        if accion=='Agregar':
            nueva_clase=Clases(nombre,profesor_id,tipo,descrip,precio,horario,opinion,clase_permanente,estatus)

            db.session.add(nueva_clase)
            db.session.commit()

            #La sesión no se cierra en esta línea porque si no, no puede construirse nvo_nombre_foto (línea 676):
            #db.session.close()

            if oferta_clases.foto_clase.data:
                nvo_nombre_foto='foto_clase_'+str(nueva_clase.id)
                db.session.close()
                foto_clase=agregar_foto(oferta_clases.foto_clase.data,nvo_nombre_foto,'static/fotos_clases',650,650)

                conexion=sqlite3.connect("data.sqlite")
                cursor=conexion.cursor()
                cursor.execute ("UPDATE clases SET foto_clase='{}' WHERE id='{}'".format(foto_clase,nueva_clase.id))
                conexion.commit()

        elif accion=='Modificar':
            clase_mod=int(clase_mod)

            conexion=sqlite3.connect("data.sqlite")
            cursor=conexion.cursor()
            cursor.execute("SELECT * FROM clases WHERE id={}".format(clase_mod))
            r=cursor.fetchall()

            if precio=="":
                precio=r[0][5]

            if horario=="":
                horario=r[0][4]

            if nombre=="":
                nombre=r[0][1]

            if tipo=="":
                tipo=r[0][2]

            if estatus=="":
                estatus=r[0][3]

            if descrip=="":
                descrip=r[0][6]

            if opinion=="":
                opinion=r[0][10]


            if oferta_clases.foto_clase.data:
                nvo_nombre_foto='foto_clase_'+str(clase_mod)
                foto_clase=agregar_foto(oferta_clases.foto_clase.data,nvo_nombre_foto,'static/fotos_clases',650,650)
                #Actualizando la imagen de la clase en la tabla "clases":
                conexion=sqlite3.connect("data.sqlite")
                cursor=conexion.cursor()
                cursor.execute ("UPDATE clases SET foto_clase='{}' WHERE id='{}'".format(foto_clase,clase_mod))
                #db.session.commit()
                conexion.commit()


            x=Clases(nombre,profesor_id,tipo,descrip,precio,horario,opinion,clase_permanente,estatus)
            conexion=sqlite3.connect("data.sqlite")
            cursor=conexion.cursor()
            cursor.execute("UPDATE clases SET clase='{}',tipo='{}',estatus='{}', horario='{}',opinion='{}', precio='{}', descrip='{}', profesor_id={}, profesor_nombre='{}', clase_permanente='{}'  WHERE id={}".format(x.clase,x.tipo,x.estatus,x.horario,x.opinion,x.precio,x.descrip,x.profesor_id,x.profesor_nombre,x.clase_permanente,clase_mod))
            conexion.commit()
            conexion.close()

        else:
            #Dejo este else para incluír la opción de eliminar en caso de ser necesario
            pass

        return redirect(url_for('lista_clases'))

    if current_user.perfil=='Alumno':
        return redirect(url_for('acceso_denegado'))
    else:
        id_clase='0'
        if accion=='Modificar':
            id_clase=clase_mod
        elif accion=='Agregar':
            try:
                id_clase=nueva_clase.id
            except UnboundLocalError:
                id_clase='0'

#Creo que esta línea debe quedar en cada condicional (Modificar o Agregar) paa poder incluir la opción "foto predeterminada" sin guardar foto en base de datos


        #foto_clase=url_for('static',filename='fotos_clases/'+foto_clase)
        return render_template('agregar_clase.html', oferta_clases=oferta_clases)

@app.route('/lista_clases')
def lista_clases():
    conexion=sqlite3.connect("data.sqlite")
    cursor=conexion.cursor()
    cursor.execute("SELECT clase FROM Clases WHERE tipo='Abierta' AND estatus='Activa'")
    clases=cursor.fetchall()

    return render_template('lista_clases.html',clases=clases)

#################################################
@app.route('/nuestras_clases/<clase_nombre>',methods=['GET','POST'])
def nuestras_clases(clase_nombre):

    conexion=sqlite3.connect("data.sqlite")
    cursor=conexion.cursor()
    cursor.execute("SELECT * FROM Clases WHERE clase='{}' AND estatus='Activa'".format(clase_nombre))
    clases=cursor.fetchall()
    descrip=clases[0][6]
    descrip=descrip.split('#')
    precio=clases[0][5]
    precio=precio.split('#')
    horario=clases[0][4]
    horario=horario.split('#')
    profesor_nombre=clases[0][8]
    profesor_id=clases[0][7]
    id_clase=str(clases[0][0])
    foto_clase=clases[0][9]
    opinion=clases[0][10]
    opinion=opinion.split('#')


    cursor.execute("SELECT cv,gen FROM Profesores WHERE id={}".format(profesor_id))
    profesor=cursor.fetchall()
    gen=profesor[0][1]
    cv=profesor[0][0]
    cv=cv.split('#')

    return render_template('nuestras_clases.html',clase_nombre=clase_nombre,descrip=descrip,profesor_nombre=profesor_nombre, cv=cv,gen=gen,precio=precio,horario=horario,id_clase=id_clase,foto_clase=foto_clase,opinion=opinion)

#####################################################
@app.route('/rentar_salon',methods=['GET','POST'])
@login_required

def rentar_salon():
    #Consulta de los profesores:
    conexion=sqlite3.connect("data.sqlite")
    cursor=conexion.cursor()
    cursor.execute("SELECT id,nombre FROM Profesores WHERE tipo='Profesor' OR tipo=='Profesor y alumno' ")
    lista_prof_disp=cursor.fetchall()
    #Consulta de los servicios
    cursor.execute('SELECT id,nombre FROM Servicios')
    lista_serv=cursor.fetchall()
    #Debido a que la consulta arroja un id en formato Integer y la forma (SelectMultipleField) requiere un tipo cadena se hace el parseo:
    lista_serv=[(str(id),nom) for id,nom in lista_serv]

    rentar_salon=Rentar_salon_forma()
    rentar_salon.profesor_id.choices=lista_prof_disp
    rentar_salon.servicios.choices=lista_serv
    usuario=current_user.usuario


    #Consulta de las clases:
    profesor_id=rentar_salon.profesor_id.data
    cursor.execute("SELECT id,clase FROM Clases WHERE profesor_id='{}'".format(profesor_id))
    lista_clases_profe=cursor.fetchall()
    rentar_salon.clase_id.choices=lista_clases_profe

    hora_inicial=rentar_salon.hora_inicial.data
    hora_final=rentar_salon.hora_final.data


    try:
        ###Calculando las horas de renta:
        try:
            hi=datetime.strptime(hora_inicial,'%H:%M')
            hf=datetime.strptime(hora_final,'%H:%M')
        except ValueError:
            hora_inicial='00:00'
            hora_final='00:00'
            hi=datetime.strptime(hora_inicial,'%H:%M')
            hf=datetime.strptime(hora_final,'%H:%M')

        hf=dt.time(hf.hour,hf.minute)
        hi=dt.time(hi.hour,hi.minute)
        n_horas=datetime.combine(date.today(),hf)-datetime.combine(date.today(),hi)
        n_horas=n_horas.total_seconds()/3600
        n_horas=round(n_horas,1)
        nh=int(n_horas)
        h=n_horas-nh
        if h>0 and h<=0.5:
            n_horas=nh+0.5
        elif h==0:
            n_horas=nh
        else:
            n_horas=nh+1

    except TypeError:
        n_horas=0

    precio_salon=rentar_salon.precio_salon.data

    if precio_salon=='' or precio_salon is None:
        precio_salon="0.0"

    precio_salon=float(precio_salon)
    try:
        salon_cant_pagar=n_horas*precio_salon

    except TypeError:
        precio_salon=0
        salon_cant_pagar=0

    rentar_salon.n_horas.choices=[(str(n_horas),str(n_horas))]

    rentar_salon.salon_cant_pagar.choices=[(str(salon_cant_pagar),str(salon_cant_pagar))]

    if rentar_salon.validate_on_submit():
        salon=rentar_salon.salon.data
        total_cant_pagar=rentar_salon.total_cant_pagar.data
        n_horas=rentar_salon.n_horas.data
        salon_cant_pagar=rentar_salon.salon_cant_pagar.data

        profesor_id=rentar_salon.profesor_id.data
        #servicios=rentar_salon.servicios.data
        s=rentar_salon.servicios.data
        #En la forma (SelectMultipleField) s se almacena como una lista. Sin embargo, la tabla "rentas" no permte este tipo de variables;
        #Por lo que la lista de servicios se convierte a cadena de texto; por ejemplo, la lista (1,3) se almacena como el texto "1,3"
        servicios=','.join(s)
        clase_id=rentar_salon.clase_id.data
        serv_cant_pagar=0

        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()

        for id in s:
            cursor.execute("SELECT id,precio FROM servicios WHERE id={}".format(id))
            r=cursor.fetchall()[0][1]
            #Se calcula el monto a pagar por hora de renta de servicios
            serv_cant_pagar=serv_cant_pagar+r
            #Se calcula el monto total a pagar por servicios por todo el tiempo de uso
        serv_cant_pagar=float(n_horas)*float(serv_cant_pagar)

        total_cant_pagar=float(salon_cant_pagar)+float(serv_cant_pagar)
        rentar_salon.serv_cant_pagar.choices=[(str(serv_cant_pagar),str(serv_cant_pagar))]
        #serv_cant_pagar=rentar_salon.serv_cant_pagar.data


        rentar_salon.total_cant_pagar.choices=[(str(total_cant_pagar),str(total_cant_pagar))]
        #total_cant_pagar=rentar_salon.total_cant_pagar.data
        accion=rentar_salon.accion.data
        dia_renta=rentar_salon.dia_renta.data
        mes_renta=rentar_salon.mes_renta.data
        anio_renta=rentar_salon.anio_renta.data
        #fec_renta=rentar_salon.fec_renta.data

        if accion=='Rentar' or accion=='Cotizar':
            mm=True

            if salon=="":
                x='Favor de indicar el salón que deseas rentar'
                m=[accion,False,x]
                flash(m)
                mm=False

            if rentar_salon.precio_salon.data=="":
                x='Indica el precio, por hora, de la renta del salón'
                m=[accion,False,x]
                flash(m)
                mm=False

            if dia_renta==[]:
                x='Indica al menos un día para reservar el salón'
                m=[accion,False,x]
                flash(m)
                mm=False

            if rentar_salon.hora_inicial.data=="":
                x='Indica la hora de inicio de la clase'
                m=[accion,False,x]
                flash(m)
                mm=False

            if rentar_salon.hora_final.data=="":
                x='Indica la hora en la que termina la clase'
                m=[accion,False,x]
                flash(m)
                mm=False

            if servicios=="":
                x='Indica al menos un paquete de servicios adicional a la renta del salón'
                m=[accion,False,x]
                flash(m)
                mm=False

        for d in dia_renta:
            fec_renta=anio_renta+'-'+mes_renta+'-'+d
            if accion=='Rentar' and mm==True:
                estatus_reserva='Vigente'
                nueva_renta=Rentas(profesor_id,salon,total_cant_pagar,precio_salon,fec_renta,n_horas,clase_id,servicios,salon_cant_pagar,serv_cant_pagar,usuario,estatus_reserva,hora_inicial,hora_final)
                #if nueva_renta.renta:
                db.session.add(nueva_renta)
                db.session.commit()
                db.session.close
                mensaje=[accion,nueva_renta.renta,nueva_renta]
                flash(mensaje)

            elif accion=='Cancelar':
                h_ini=hora_inicial+':00.000000'
                h_fin=hora_final+':00.000000'
                conexion=sqlite3.connect("data.sqlite")
                cursor=conexion.cursor()
                #cursor.execute("UPDATE alumnos SET nombre='{}',sangre='{}', seguro='{}', email='{}', cel='{}',contacto_emergencia='{}', tel_emergencia='{}', gen='{}',observaciones='{}', fec_nac='{}' WHERE id={}".format(nombre,sangre,seguro,email,cel,contacto_emergencia,tel_emergencia,gen,observaciones,fec_nac,alumno_mod))
                #cursor.execute("DELETE FROM rentas WHERE profesor_id={} AND fec_renta='{}' AND hora_inicial='{}' AND hora_final='{}' AND salon='{}'".format(profesor_id,fec_renta,h_ini,h_fin,salon))
                cursor.execute("UPDATE rentas SET estatus_reserva='Cancelada', usuario='{}' WHERE profesor_id={} AND fec_renta='{}' AND hora_inicial='{}' AND hora_final='{}' AND salon='{}'".format(usuario,profesor_id,fec_renta,h_ini,h_fin,salon))

                conexion.commit()
                x="Se canceló la renta del salón {} para el día {} de {} a {}".format(salon,fec_renta,hora_inicial,hora_final)
                mensaje=[accion,'',x]
                flash(mensaje)

            elif accion=='Cotizar':
                try:
                    h_ini=hora_inicial+':00.000000'
                    h_fin=hora_final+':00.000000'
                    conexion=sqlite3.connect("data.sqlite")
                    cursor=conexion.cursor()
                    cursor.execute("SELECT id,hora_inicial,hora_final FROM rentas WHERE estatus_reserva='Vigente' AND salon='{}' AND fec_renta='{}' AND ((hora_inicial<='{}' AND hora_final>'{}') OR (hora_inicial<'{}' AND hora_final>='{}') OR (hora_inicial>'{}' AND hora_final<'{}'))".format(salon,fec_renta,h_ini,h_ini,h_fin,h_fin,h_ini,h_fin))
                    r=cursor.fetchall()
                    hora_inicial=r[0][1]
                    hora_final=r[0][2]
                #Quitándole la terminación :00.000000 a la cadena de texto consultada
                    hora_inicial=hora_inicial[:5]
                    hora_final=hora_final[:5]

                except IndexError:
                    renta=True
                    #h_ini=hora_inicial[:5]
                    #h_fin=hora_final[:5]
                    x='El salón {} se encuentra disponible para el día {} entre las {} y las {}'.format(salon,fec_renta,rentar_salon.hora_inicial.data,rentar_salon.hora_final.data)
                    mensaje=[accion,renta,x]
                    flash(mensaje)


                else:
                    renta=False
                    x="El salón {} no está disponible para el día {}. Se encuentra reservado de las {} a las {}.".format(salon,fec_renta,hora_inicial,hora_final)
                    mensaje=[accion,renta,x]
                    flash(mensaje)


        #return render_template('renta.html',nueva_renta=nueva_renta)

    if current_user.perfil=='Alumno':
        return redirect(url_for('acceso_denegado'))
    else:
        return render_template('rentar_salon.html', rentar_salon=rentar_salon)


#################
@app.route('/comprar_clases',methods=['GET','POST'])
@login_required

def comprar_clases():

    conexion=sqlite3.connect("data.sqlite")
    cursor=conexion.cursor()
    cursor.execute("SELECT id,clase FROM Clases WHERE estatus='Activa'")
    lista_clases_disp=cursor.fetchall()


#    clases_disp=Clases.query.all()
#    lista_clases_disp=[(c.id,c.clase) for c in clases_disp]

    comprar_clases=Comprar_clases_forma()
#Pasando los valores de la consulta (clases_disp) para llenar las opciones del SelectField "clase_id"
    comprar_clases.clase_id.choices=lista_clases_disp

    busca_alumno=comprar_clases.busca_alumno.data
    conexion=sqlite3.connect("data.sqlite")
    cursor=conexion.cursor()
    cursor.execute("SELECT id,nombre FROM Profesores WHERE (tipo='Alumno' OR tipo=='Profesor y alumno') AND nombre LIKE '%{}%'".format(busca_alumno))
    lista_alumnos_posibles=cursor.fetchall()
    comprar_clases.alumno_id.choices=lista_alumnos_posibles
    clase_id=comprar_clases.clase_id.data

    conexion=sqlite3.connect("data.sqlite")
    cursor=conexion.cursor()
    cursor.execute("SELECT cantidad,cantidad FROM precio_clases WHERE clase_id='{}'".format(clase_id))
    lista_n_clases=cursor.fetchall()
    lista_n_clases=[(str(cantidad),cantidad) for cantidad,cantidad in lista_n_clases]

    comprar_clases.n_clases.choices=lista_n_clases
    n_clases=comprar_clases.n_clases.data

    try:
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute("SELECT precio FROM precio_clases WHERE clase_id='{}' AND cantidad={} ".format(clase_id,n_clases))
        precio_tarjeta=cursor.fetchall()[0][0]

    except sqlite3.OperationalError:
        precio_tarjeta=0

    precio=precio_tarjeta

    if comprar_clases.validate_on_submit():
        alumno_id=comprar_clases.alumno_id.data
        #precio_tarjeta=comprar_clases.precio_tarjeta.data
        fec_vencimiento=comprar_clases.fec_vencimiento.data

        paquete_clases=Tarjetas(alumno_id,clase_id,n_clases,precio_tarjeta,fec_vencimiento)
        if paquete_clases.tar_creada:
            db.session.add(paquete_clases)
            db.session.commit()

        compra=paquete_clases.tar_creada

        return render_template('compra.html',tarjeta=paquete_clases, compra=compra, precio=precio)

    if current_user.perfil=='Alumno':
        return redirect(url_for('acceso_denegado'))
    else:
        return render_template('comprar_clases.html', comprar_clases=comprar_clases, precio=precio)


@app.route('/asistencia',methods=['GET','POST'])
@login_required

def asistencia():

    conexion=sqlite3.connect("data.sqlite")
    cursor=conexion.cursor()
    cursor.execute('SELECT id,clase FROM Clases WHERE id>0')
    lista_clases_disp=cursor.fetchall()

    asistencia=Pase_lista_forma()
#Pasando los valores de la consulta (clases_disp) para llenar las opciones del SelectField "clase_id"
    asistencia.clase_id.choices=lista_clases_disp

    busca_alumno=asistencia.busca_alumno.data
    conexion=sqlite3.connect("data.sqlite")
    cursor=conexion.cursor()
    cursor.execute("SELECT id,nombre FROM Profesores WHERE (tipo='Alumno' OR tipo=='Profesor y alumno') AND nombre LIKE '%{}%'".format(busca_alumno))
    lista_alumnos_posibles=cursor.fetchall()
    asistencia.alumno_id.choices=lista_alumnos_posibles


    if asistencia.validate_on_submit():
        alumno_id=asistencia.alumno_id.data
        clase_id=asistencia.clase_id.data

        pase_lista=Pase_lista(alumno_id,clase_id)

        return render_template('asistencia.html',pase_lista=pase_lista)

    #clases=Clases.query.all()

    if current_user.perfil=='Alumno':
        return redirect(url_for('acceso_denegado'))
    else:
        return render_template('registrar_asistencia.html', asistencia=asistencia)



#######################################################
@app.route('/agregar_servicio',methods=['GET','POST'])
@login_required

def agregar_servicio():

    agregar_servicio=Agregar_servicio_forma()

    if agregar_servicio.validate_on_submit():
        nombre=agregar_servicio.nombre.data
        precio=agregar_servicio.precio.data
        descrip=agregar_servicio.descrip.data

        nuevo_servicio=Servicios(nombre,descrip,precio)
        db.session.add(nuevo_servicio)
        db.session.commit()
        db.session.close

        return render_template('servicio.html',nuevo_servicio=nuevo_servicio)

    if current_user.perfil=='Alumno':
        return redirect(url_for('acceso_denegado'))
    else:
        return render_template('agregar_servicio.html', agregar_servicio=agregar_servicio)


@app.route('/agregar_material',methods=['GET','POST'])
@login_required

def agregar_material():


    agregar_material=Agregar_material_forma()
    accion=agregar_material.accion.data

    if accion=='Modificar':
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute("SELECT id,nombre FROM Materiales")
        lista_mat=cursor.fetchall()
        lista_mat=[(str(id),nombre) for id,nombre in lista_mat]
        agregar_material.material_mod.choices=lista_mat


    if agregar_material.validate_on_submit():
        nombre=agregar_material.nombre.data
        n_piezas=agregar_material.n_piezas.data
        estado=agregar_material.estado.data
        fec_mantenimiento=agregar_material.fec_mantenimiento.data
        observaciones=agregar_material.observaciones.data


        if accion=='Agregar':
            try:
                agregar_material.checar_material(nombre)
            except ValidationError:
                x='Este material ya se encuentra registrado'
                mensaje=[False,x]
                flash(mensaje)

            else:
                nuevo_material=Materiales(nombre,n_piezas,fec_mantenimiento,estado,observaciones)

                if nuevo_material.nombre is None:
                    x='El material no pudo ingresarse. Favor de revisar la información '
                    mensaje=[False,x]
                    flash(mensaje)
                else:
                    db.session.add(nuevo_material)
                    db.session.commit()
                    db.session.close
                    x='Se registró el material en el inventario'
                    mensaje=[True,x]
                    flash(mensaje)


        elif accion=='Modificar':
            material_mod=agregar_material.material_mod.data
            material_mod=int(material_mod)


            conexion=sqlite3.connect("data.sqlite")
            cursor=conexion.cursor()
            cursor.execute("SELECT * FROM materiales WHERE id={}".format(material_mod))
            r=cursor.fetchall()

            if nombre=="":
                nombre=r[0][1]

            if n_piezas=="":
                n_piezas=r[0][2]

            if fec_mantenimiento=="":
                fec_mantenimiento=r[0][3]
            else:
                try:
                    fm=datetime.strptime(fec_mantenimiento,'%Y-%m-%d')
                except ValueError:
                    x='El formato de la fecha debe ser AAAA-MM-DD; por ejemplo, 2016-08-24'
                    mensaje=[False,x]
                    flash(mensaje)
                    alerta_fecha=True
                else:
                    fec_mantenimiento=dt.date(fm.year,fm.month,fm.day)
                    alerta_fecha=False

            if estado=="":
                estado=r[0][4]

            if observaciones=="":
                observaciones=r[0][5]

            conexion=sqlite3.connect("data.sqlite")
            cursor=conexion.cursor()
            print('Fecha de mantenimiento:')
            print(fec_mantenimiento)

            cursor.execute("UPDATE materiales SET nombre='{}',n_piezas={}, fec_mantenimiento='{}', estado='{}', observaciones='{}' WHERE id={}".format(nombre,n_piezas,fec_mantenimiento,estado,observaciones,material_mod))
            if alerta_fecha:

                conexion.close()
                x='La información no se pudo actualizar. Favor de revisar su captura'
                mensaje=[False,x]
                flash(mensaje)
            else:
                conexion.commit()
                conexion.close()
                x='El inventario fue modificado'
                mensaje=[True,x]
                flash(mensaje)

        else:
            #Dejo este else para incluír la opción de eliminar en caso de ser necesario
            pass



        #return render_template('materiales.html',nuevo_material=nuevo_material)

    if current_user.perfil=='Alumno':
        return redirect(url_for('acceso_denegado'))
    else:
        return render_template('agregar_material.html', agregar_material=agregar_material)


@app.route('/armar_paquete',methods=['GET','POST'])
@login_required

def editar_paquete_servicios():
#Función que devuelve el contenido de un paquete de servicios
    def contenido_paquete(servicio_id):
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute("SELECT material_nombre FROM serv_componentes WHERE servicio_id={}".format(servicio_id))
        #r=cursor.fetchall()
        contenido=cursor.fetchall()
        return contenido


    conexion=sqlite3.connect("data.sqlite")
    cursor=conexion.cursor()

    cursor.execute('SELECT id,nombre FROM servicios')
    lista_serv_disp=cursor.fetchall()
    cursor.execute('SELECT id,nombre FROM materiales WHERE n_piezas>0')
    lista_mat=cursor.fetchall()

    #Debido a que la consulta arroja un id en formato Integer y la forma (SelectMultipleField) requiere un tipo cadena se hace el parseo:
    lista_mat=[(str(id),nom) for id,nom in lista_mat]

    editar_paquete=Armar_paquete_servicios_forma()
    editar_paquete.servicio_id.choices=lista_serv_disp

    editar_paquete.material_id.choices=lista_mat
    accion=editar_paquete.accion.data
    contenido=[]

    if editar_paquete.validate_on_submit():
        servicio_id=editar_paquete.servicio_id.data

        if accion=='Contenido':
            contenido=contenido_paquete(servicio_id)

        elif accion=='Agregar':
            material_id=editar_paquete.material_id.data
            l_mat=[int(id) for id in material_id]
            for material_id in l_mat:
                nuevo_registro=Serv_componentes(servicio_id,material_id)
                nuevo_registro.agregar_material()

            contenido=contenido_paquete(servicio_id)

        elif accion=='Eliminar':
            material_id=editar_paquete.material_id.data

            l_mat=[int(id) for id in material_id]
            for material_id in l_mat:
                registro_borrar=Serv_componentes(servicio_id,material_id)
                registro_borrar.retirar_material()

            contenido=contenido_paquete(servicio_id)


    if current_user.perfil=='Alumno':
        return redirect(url_for('acceso_denegado'))
    else:
        return render_template('editar_paquete.html', editar_paquete=editar_paquete, contenido=contenido)


######################################################


@app.route('/editar_permisos',methods=['GET','POST'])
@login_required

def editar_permisos():
#Función que devuelve el contenido de un paquete de servicios
    def contenido_grupo(perfil):
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute("SELECT usuario FROM usuarios WHERE perfil='{}'".format(perfil))

        contenido=cursor.fetchall()
        return contenido

    def cambiar_perfil(id,perfil):
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute("UPDATE usuarios SET perfil='{}' WHERE id={}".format(perfil,id))
        conexion.commit()

    conexion=sqlite3.connect("data.sqlite")
    cursor=conexion.cursor()
    cursor.execute('SELECT id,usuario FROM usuarios')
    lista_usuarios=cursor.fetchall()

    #Debido a que la consulta arroja un id en formato Integer y la forma (SelectMultipleField) requiere un tipo cadena se hace el parseo:
    lista_usuarios=[(str(id),usuario) for id,usuario in lista_usuarios]

    editar_grupo=Cambiar_perfil_forma()

    editar_grupo.usuario.choices=lista_usuarios
    accion=editar_grupo.accion.data
    contenido=[]

    if editar_grupo.validate_on_submit():
        perfil=editar_grupo.perfil.data

        if accion=='Contenido':
            contenido=contenido_grupo(perfil)

        elif accion=='Cambiar':
            l_usuarios=editar_grupo.usuario.data
            l_usuarios=[int(id) for id in l_usuarios]
            for id in l_usuarios:
                cambiar_perfil(id,perfil)

            contenido=contenido_grupo(perfil)

    if current_user.perfil=='Directivo':
        return render_template('editar_permisos.html', editar_grupo=editar_grupo, contenido=contenido)
    else:
        return redirect(url_for('acceso_denegado'))


@app.route('/agregar_concepto',methods=['GET','POST'])
@login_required

def agregar_concepto():

    agregar_concepto=Agregar_concepto_forma()
    accion=agregar_concepto.accion.data


    if accion=='Eliminar':
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute("SELECT id,concepto FROM Conceptos")
        lista_conceptos=cursor.fetchall()
        lista_conceptos=[(str(id),concepto) for id,concepto in lista_conceptos]
        agregar_concepto.concepto_mod.choices=lista_conceptos


    if agregar_concepto.validate_on_submit():

        if accion=="Agregar":
            concepto=agregar_concepto.concepto.data

            if concepto=='':
                x="Por favor, indica el concepto que deseas agregar"
                mensaje=[False,x]
                flash(mensaje)

            else:

                try:
                    agregar_concepto.checar_concepto(concepto)
                except ValidationError:
                    x='Este concepto ya fue registrado anteriormente.'
                    mensaje=[False,x]
                    flash(mensaje)

                else:
                    nuevo_concepto=Conceptos(concepto)

                    db.session.add(nuevo_concepto)
                    db.session.commit()
                    db.session.close
                    x='Concepto nuevo agregado al catálogo: {}'.format(concepto)
                    mensaje=[True,x]
                    flash(mensaje)


        elif accion=='Eliminar':
            print('Cero')
            concepto_mod=agregar_concepto.concepto_mod.data

            conexion=sqlite3.connect("data.sqlite")
            cursor=conexion.cursor()
            cursor.execute("SELECT concepto FROM Conceptos WHERE id='{}'".format(concepto_mod))
            r=cursor.fetchall()
            concepto_eliminado=r[0][0]

            cursor.execute("DELETE FROM Conceptos WHERE id='{}'".format(concepto_mod))
            conexion.commit()
            x='Concepto eliminado del catálogo: {}'.format(concepto_eliminado)
            mensaje=[True,x]
            flash(mensaje)



    if current_user.perfil!='Directivo':
        return redirect(url_for('acceso_denegado'))
    else:
        return render_template('agregar_concepto.html', agregar_concepto=agregar_concepto)


@app.route('/agregar_movimiento',methods=['GET','POST'])
@login_required

def agregar_movimiento():


    #¿Por qué paga?

    conexion=sqlite3.connect("data.sqlite")
    cursor=conexion.cursor()
    cursor.execute('SELECT id,concepto FROM conceptos')
    lista_conceptos=cursor.fetchall()
    lista_conceptos=[(str(id),con) for id,con in lista_conceptos]


    agregar_movimiento=Agregar_movimientos_forma()
    perfil=agregar_movimiento.perfil.data

    #¿Quién paga?
    conexion=sqlite3.connect("data.sqlite")
    cursor=conexion.cursor()
    if perfil=="Profesor" or perfil=="Alumno":
        cursor.execute("SELECT id,nombre FROM profesores WHERE tipo='{}' OR tipo='Profesor y alumno'".format(perfil))
    else:
        cursor.execute("SELECT id,nombre FROM profesores WHERE tipo='{}'".format(perfil))

    lista_personas=cursor.fetchall()
    lista_personas=[(str(id),nom) for id,nom in lista_personas]

    agregar_movimiento.nombre.choices=lista_personas
    agregar_movimiento.concepto.choices=lista_conceptos

    if agregar_movimiento.validate_on_submit():


        mov=agregar_movimiento.mov.data
        monto=agregar_movimiento.monto.data
        descrip=agregar_movimiento.descrip.data
        folio=agregar_movimiento.folio.data
        usuario=current_user.usuario


        nombre=agregar_movimiento.nombre.data
        nombre=int(nombre)
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute('SELECT nombre,tipo FROM profesores WHERE id={}'.format(nombre))
        r=cursor.fetchall()
        nombre=r[0][0]
        tipo=r[0][1]

        concepto=agregar_movimiento.concepto.data
        concepto=int(concepto)
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute('SELECT concepto FROM Conceptos WHERE id={}'.format(concepto))
        concepto=cursor.fetchall()[0][0]

        nuevo_mov=Movimientos(nombre,tipo,usuario,mov,monto,concepto,descrip,folio)
        db.session.add(nuevo_mov)
        db.session.commit()
        db.session.close
        x='{} registró una {} nueva por un monto de {} pesos correspondiente a {}'.format(nombre,mov,monto,concepto)
        mensaje=[True,x]
        flash(mensaje)

    if current_user.perfil=='Alumno':
        return redirect(url_for('acceso_denegado'))
    else:
        return render_template('agregar_movimiento.html', agregar_movimiento=agregar_movimiento)

@app.route('/asignar_precios_clase',methods=['GET','POST'])
@login_required

def asignar_precios_clase():
#Función que devuelve el contenido de un paquete de servicios
    def precios_clase(clase_id):
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute("SELECT precio,cantidad,vigencia FROM precio_clases WHERE clase_id={}".format(clase_id))

        precios=cursor.fetchall()
        return precios

    conexion=sqlite3.connect("data.sqlite")
    cursor=conexion.cursor()

    cursor.execute('SELECT id,clase FROM clases')
    lista_clases=cursor.fetchall()
    lista_clases=[(str(id),clase) for id,clase in lista_clases]

    agregar_precio=Agregar_precio_clase_forma()
    agregar_precio.clase_id.choices=lista_clases

    accion=agregar_precio.accion.data
    precios=[]

    if agregar_precio.validate_on_submit():
        clase_id=agregar_precio.clase_id.data
        clase_id=int(clase_id)

        if accion=='Contenido':
            precios=precios_clase(clase_id)

        elif accion=='Agregar':
            cantidad=agregar_precio.cantidad.data
            precio=agregar_precio.precio.data
            vigencia=agregar_precio.vigencia.data

            try:
                nuevo_precio=Precio_clases(clase_id,precio,cantidad,vigencia)
                nuevo_precio.agregar_precio()
            except sqlite3.IntegrityError:
                x='Este precio ya se encuentra registrado. Si deseas modificar la vigencia elimina el precio y vuelve a ingresarlo con la vigencia deseada.'
                mensaje=[False,x]
                flash(mensaje)
            else:
                x='Precio registrado correctamente'
                mensaje=[True,x]
                flash(mensaje)

        elif accion=='Eliminar':
            cantidad=agregar_precio.cantidad.data
            precio=agregar_precio.precio.data
            vigencia=agregar_precio.vigencia.data

            precio_borrar=Precio_clases(clase_id,precio,cantidad,vigencia)
            precio_borrar.retirar_precio()
            x='Precio eliminado'
            mensaje=[True,x]
            flash(mensaje)

        precios=precios_clase(clase_id)


    if current_user.perfil=='Alumno':
        return redirect(url_for('acceso_denegado'))
    else:
        return render_template('asignar_precios_clase.html', agregar_precio=agregar_precio, precios=precios)

######################################################

@app.route('/definir_reportes',methods=['GET','POST'])
@login_required

def definir_reportes():

    definir_reportes=Definir_reportes_forma()

    if definir_reportes.validate_on_submit():
        reporte=definir_reportes.reporte.data
        tabla=definir_reportes.tabla.data
        encabezados=definir_reportes.encabezados.data

        nuevo_reporte=Reportes_generales(reporte,encabezados,tabla)
        db.session.add(nuevo_reporte)
        db.session.commit()
        db.session.close
        x='Se registró el reporte: {}'.format(reporte)
        mensaje=[True,x]
        flash(mensaje)

    if current_user.perfil=='Alumno':
        return redirect(url_for('acceso_denegado'))
    else:
        return render_template('definir_reportes.html', definir_reportes=definir_reportes)


@app.route('/reportes',methods=['GET','POST'])
@login_required

def reportes():
    conexion=sqlite3.connect("data.sqlite")
    cursor=conexion.cursor()
    cursor.execute("SELECT id,reporte FROM reportes_generales")
    lista_reportes=cursor.fetchall()
    lista_reportes=[(str(id),reporte) for id,reporte in lista_reportes]

    generar_reportes=Reportes_forma()
    generar_reportes.reporte.choices=lista_reportes

    if generar_reportes.validate_on_submit():
        reporte=generar_reportes.reporte.data
        #ruta=generar_reportes.ruta.data
        #nombre_archivo=generar_reportes.nombre_archivo.data


        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        cursor.execute("SELECT * FROM 'reportes_generales' WHERE id={}".format(reporte))
        r=cursor.fetchall()
        tabla=r[0][3]
        encabezado=r[0][2]
        nombre_archivo=r[0][1]
        nombre_archivo=nombre_archivo+'.csv'

### Líneas para editar los reportes a mano:
        #if reporte=='4':
        #    cursor.execute("DELETE FROM materiales WHERE id=39")
        #    cursor.execute("DELETE FROM usuarios WHERE id=6")
        #    cursor.execute("DELETE FROM clases WHERE id=23")
        #    cursor.execute("UPDATE reportes_generales SET encabezados='Identificador de la clase,Clase,Tipo,Estatus,Horario,Precio,Descripción,Identificador del profesor,Profesor,Foto,Opiniones,Temporalidad de la clase' WHERE id=7")
        #    cursor.execute("UPDATE reportes_generales SET encabezados='id,Nombre,Tipo,Usuario que realiza la operación,Tipo de movimiento,Monto,Fecha del movimiento,Concepto,Descripción,Folio recibo' WHERE id=3")
        #    cursor.execute("DELETE FROM movimientos WHERE id=64")


        #    conexion.commit()



        encabezado=tuple(encabezado.split(','))

        #y=[]
        #y.append(encabezado)
        #encabezado=y

        #Consulta de la tabla para hacer un reporte general
        conexion=sqlite3.connect("data.sqlite")
        cursor=conexion.cursor()
        #El reporte con id==3 es el reporte de "Entradas y salidas", le cual debe censurarse para
        # que los usuarios solo puedan consultar su información; excepto el personal administrativo, el cual
        # debería poder ver toda la información:
        if reporte=='3':
            if current_user.perfil=='Directivo':
                #cursor.execute("SELECT * FROM '{}'".format(tabla))
                consulta="SELECT * FROM {}".format(tabla)
                df=pd.read_sql(consulta,conexion)
            else:
                #cursor.execute("SELECT * FROM '{}' WHERE usuario='{}'".format(tabla,current_user.usuario))
                consulta="SELECT * FROM {} WHERE usuario='{}'".format(tabla,current_user.usuario)
                df=pd.read_sql(consulta,conexion)

        elif reporte=='8':
            #PAGOS:
            consulta="SELECT nombre,monto,mov,fec_mov FROM movimientos"
            #Se crea un dataframe con la consulta y la conexión a la base de datos:
            df=pd.read_sql(consulta,conexion)
            #Definimos pagosdf para quedarnos solo con las Entradas:
            pagosdf=df[df['mov']=='Entrada']

            #Nos quedamos con el mes y el año del movimiento para calcular los movimientos mensuales usando una función lambda.
            pagosdf['periodo']=pagosdf['fec_mov'].apply(lambda f: f[:7])
            pagosdf['profesor_nombre']=pagosdf['nombre']

            #Redefinimos pagosdf para quedarnos solo con las variales de interés; agrupar haciendo una suma del monto por nombe y periodo.
            pagosdf=pagosdf[['profesor_nombre','periodo','monto']].groupby(['profesor_nombre','periodo']).sum()
            #Se usa le método reset_index() para que las variables 'profesor_nombre' y 'periodo' dejen de ser índices del dataframe
            #y se conviertan en columnas del dataframe. NOTA: estas dos variables eran columnas que se volvieron índices al usar el método groupby
            pagosdf=pagosdf.reset_index()

            #DEUDA:
            consulta="SELECT total_cant_pagar,fec_renta,n_horas,profesor_nombre,estatus_reserva FROM rentas"
            df=pd.read_sql(consulta,conexion)
            deudadf=df[df['estatus_reserva']=='Vigente']
            deudadf['periodo']=deudadf['fec_renta'].apply(lambda f: f[:7])
            deudadf=deudadf[['profesor_nombre','periodo','total_cant_pagar']].groupby(['profesor_nombre','periodo']).sum()
            #Se usa le método reset_index() para que las variables 'profesor_nombre' y 'periodo' dejen de ser índices del dataframe
            #y se conviertan en columnas del dataframe. NOTA: estas dos variables eran columnas que se volvieron índices al usar el método groupby
            deudadf=deudadf.reset_index()


            df=pd.merge(deudadf,pagosdf,how='outer',on=['profesor_nombre','periodo'])
            #Se crea una columna de advertencias:
            #df['advertencia'] = ['Registrar rentas' if x == True else '' for x in df['total_cant_pagar'].isna()]
            df['advertencia']=np.where(df['total_cant_pagar'].isna(),'Registro de rentas pendiente','')
            cols = list(df.columns.values)
            #Remplazamos NaN por ceros para poder calcular el saldo de la deuda:
            df['total_cant_pagar'].fillna(0, inplace=True)
            df['monto'].fillna(value=0, inplace=True)

            #Se calcula el saldo de deuda
            df['saldo_deuda']=df['total_cant_pagar']-df['monto']
            #Se completan las observaciones restantes de la columna advertencia con el método select en lugar del método where:
            condiciones=[(df['advertencia']!='Registro de rentas pendiente') & (df['saldo_deuda']<0),
                        (df['advertencia']!='Registro de rentas pendiente') & (df['saldo_deuda']==0),
                        (df['advertencia']!='Registro de rentas pendiente') & (df['saldo_deuda']>0),
                        (df['advertencia']=='Registro de rentas pendiente')]
            asignacion=['Saldo a favor','Sin adeudo','Con adeudo','Registro de rentas pendiente']
            df['advertencia']=np.select(condiciones,asignacion)


            df=df[['periodo','profesor_nombre','total_cant_pagar','monto','saldo_deuda','advertencia']]

            if current_user.perfil!='Directivo':
                df['total_cant_pagar']='Usted no tienen permisos para ver esta información'
                df['monto']='Usted no tienen permisos para ver esta información'
                df['saldo_deuda']='Usted no tienen permisos para ver esta información'

        else:
            #cursor.execute("SELECT * FROM '{}'".format(tabla))
            consulta="SELECT * FROM {}".format(tabla)
            df=pd.read_sql(consulta,conexion)

        #r=cursor.fetchall()

        #En lugar de exportar el CSV a una carpeta del servidor se descarga, por eso debo escribir el archivo línea por línea:
        def generar():
            data = StringIO()
            w = csv.writer(data)
            # write header
            #w.writerow(('action', 'timestamp'))
            w.writerow(encabezado)
            yield data.getvalue()
            data.seek(0)
            data.truncate(0)
            # write each log item
            #for item in log:
            #Se usa el método itertuples con index=False para no incluir el índice en el archivo
            for item in df.itertuples(index=False):
                w.writerow(item)
                yield data.getvalue()
                data.seek(0)
                data.truncate(0)

###############################################################################
###ESTA SECCIÓN COMENTADA SIRVE PARA GUARDAR ARCHIVOS LOCALMENTE:
        #for i in range(0,len(r)):
        #    encabezado.append(r[i])

        #archivo=open(ruta+'/'+nombre_archivo+'.csv','w')
        #with archivo:
        #    writer=csv.writer(archivo)
        #    writer.writerows(encabezado)
        #    x='El archivo {} se guardó en la ruta indicada'.format(nombre_archivo)
        #    mensaje=[True,x]
        #    flash(mensaje)
##############################################################################
        # add a filename
        #Comentar desde aquí para pruebas:
        headers = Headers()
        headers.set('Content-Disposition', 'attachment', filename=nombre_archivo)

        # stream the response as the data is generated
        return Response(
            stream_with_context(generar()),
            mimetype='text/csv', headers=headers
        )
###################################################################################
    if current_user.perfil=='Alumno':
        return redirect(url_for('acceso_denegado'))
    else:
        return render_template('reportes.html', generar_reportes=generar_reportes)


@app.route('/config_email',methods=['GET','POST'])
@login_required

def config_email():

    config_email=Config_email_inst_forma()

    if config_email.validate_on_submit():
        email=config_email.email.data
        password=config_email.password.data

        if config_email.checar_email():
            conexion=sqlite3.connect("data.sqlite")
            cursor=conexion.cursor()
            cursor.execute("UPDATE config_email_inst SET email='{}',password='{}' WHERE id=1".format(email,password))
            conexion.commit()
            x='Se modificó la cuenta institucional: {}'.format(email)
            mensaje=[True,x]
            flash(mensaje)

        else:
            nueva_cuenta=Config_email_inst(email,password)
            db.session.add(nueva_cuenta)
            db.session.commit()
            db.session.close
            x='Se registró la cuenta institucional: {}'.format(email)
            mensaje=[True,x]
            flash(mensaje)

    if current_user.perfil=='Alumno':
        return redirect(url_for('acceso_denegado'))
    else:
        return render_template('config_email.html', config_email=config_email)



@app.route('/config_fondo',methods=['GET','POST'])
@login_required

def config_fondo():

    config_fondo=Config_fondo_principal_forma()

    if config_fondo.validate_on_submit():
        if config_fondo.fondo_principal.data:
            fondo_principal=agregar_foto(config_fondo.fondo_principal.data,'fondo_principal','static',0,0)
            x='Si no puede ver los cambios actualice el caché de su navegador: Para Windows/Linux presiones Crtl+F5; para Mac presione Cmd+Shift+R'
            mensaje=[True,x]
            flash(mensaje)


    if current_user.perfil=='Alumno':
        return redirect(url_for('acceso_denegado'))
    else:
        return render_template('config_fondo.html', config_fondo=config_fondo)

######################################################
if __name__=='__main__':
    app.run(debug=True)


#######################################################
