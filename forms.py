#FORMS:

from flask_wtf import FlaskForm
#from wtforms_sqlalchemy.fields import QuerySelectField
from modelos import Clases

from wtforms import ValidationError,StringField, IntegerField, SubmitField, IntegerField, FloatField, SelectField, RadioField,TextAreaField,SelectMultipleField,PasswordField
from wtforms.validators import InputRequired,Email,EqualTo,DataRequired
from modelos import *
from flask_wtf.file import FileField,FileAllowed
#(Sirven para ingresar las fotos del background y de las clases)


class Login_forma(FlaskForm):
    email=StringField('Correo electrónico:',validators=[DataRequired(),Email()])
    password=PasswordField('Contraseña:',validators=[DataRequired()])
    ingresar=SubmitField('Ingresar')


class Recuperar_password_forma(FlaskForm):
    email=StringField('Correo electrónico:',validators=[DataRequired(),Email()])
    #password=PasswordField('Contraseña:',validators=[DataRequired()])
    recuperar=SubmitField('Recuperar')

    def revisar_email(self,email):
        if Usuarios.query.filter_by(email=email).first() is None:
            raise ValidationError('Este correo no se ha registrado. Favor de revisarlo.')

class Registro_forma(FlaskForm):
    email=StringField('Correo electrónico:',validators=[DataRequired(),Email()])
    usuario=StringField('Usuario:',validators=[DataRequired()])
    password=PasswordField('Contraseña:',validators=[DataRequired(),EqualTo('confirm_password',message='¡La contraseña debe coincidir!')])
    confirm_password=PasswordField('Confirmar contraseña:',validators=[DataRequired()])
    registrar=SubmitField('Registrarse')

    def checar_email(self,email):
        if Usuarios.query.filter_by(email=email).first():
            raise ValidationError('Este correo ya fue registrado. Proporciona otro o recupera tu contraseña')

    def checar_usuario(self,usuario):
        if Usuarios.query.filter_by(usuario=usuario).first():
            raise ValidationError('Este usuario ya fue registrado. Proporciona otro o recupera tu contraseña')

class Cambiar_password_forma(FlaskForm):
    email=StringField('Correo electrónico:',validators=[DataRequired(),Email()])
    ant_password=PasswordField('Contraseña anterior:',validators=[DataRequired()])
    nvo_password=PasswordField('Contraseña nueva:',validators=[DataRequired(),EqualTo('confirm_nvo_password',message='¡La contraseña nueva debe coincidir!')])
    confirm_nvo_password=PasswordField('Confirmar contraseña nueva:',validators=[DataRequired()])
    cambiar=SubmitField('Cambiar')

    def revisar_email(self,email):
        if Usuarios.query.filter_by(email=email).first() is None:
            raise ValidationError('Este correo no se ha registrado. Favor de revisarlo.')


class Agregar_alumno_forma(FlaskForm):
    nombre=StringField('Nombre completo:')
    email=StringField('Correo electrónico:')
    cel=StringField('Teléfono móvil:')
    contacto_emergencia=StringField('Contacto de emergencia:')
    tel_emergencia=StringField('Teléfono de emergencia:')
    gen=RadioField('Género:',choices=[('Femenino','Femenino'),('Masculino','Masculino'),('Otro','Otro')],validators=[InputRequired('Indica el género')])
    fec_nac=StringField('Fecha de nacimiento:')
    #fec_insc=StringField('Fecha de inscripción:')
    observaciones=TextAreaField('¿Hay algo que debamos saber de ti?')
    agregar=SubmitField('Realizar')
    sangre=SelectField('Tipo de sangre:',choices=[('',''),('O+','O+'),('O-','O-'),('A+','A+'),('A-','A-'),('B+','B+'),('B-','B-'),('AB+','AB+'),('AB-','AB-')])
    seguro=SelectField('Aportación al seguro contra accidentes:',choices=[('',''),('Sí','Sí'),('No','No')])
    accion=RadioField('¿Qué deseas hacer?',choices=[('Modificar','Modificar expediente del alumno'),('Agregar','Inscribir a un alumno nuevo')])
    alumno_mod=SelectField('¿Cuál alumno deseas modificar?',choices=[('0','')])

    def checar_email(self,email):
        if Alumnos.query.filter_by(email=email).first():
            raise ValidationError('Este correo ya fue registrado anteriormente.')

    def checar_alumno(self,nombre):
        if Alumnos.query.filter_by(nombre=nombre).first():
            raise ValidationError('Este alumno ya fue registrado anteriormente. No es necesario que lo vuelvas a hacer.')


class Agregar_profesor_forma(FlaskForm):
    nombre=StringField('Nombre completo:')
    tutor=StringField('Nombre del tutor:')
    tipo=RadioField('',choices=[('Alumno','Alumno'),('Profesor','Profesor'),('Profesor y alumno','Profesor y alumno'),('Administrativo','Equipo AEM'),('Proveedor','Proveedor')],validators=[InputRequired('Indica si deseas inscribir un alumno o profesor')])
    email=StringField('Correo electrónico:')
    cel=StringField('Teléfono móvil:')
    contacto_emergencia=StringField('Contacto de emergencia:')
    tel_emergencia=StringField('Teléfono de emergencia:')
    gen=RadioField('Género:',choices=[('Femenino','Femenino'),('Masculino','Masculino'),('Otro','Otro')],validators=[InputRequired('Indica el género')])
    #fec_nac=StringField('Fecha de nacimiento:')
    #fec_insc=StringField('Fecha de inscripción:')
    observaciones=TextAreaField('¿Hay algo que debamos saber sobre tu salud?')
    cv=TextAreaField('Proporciona una breve semblanza de tu carrera profesional:')
    agregar=SubmitField('Realizar')
    sangre=SelectField('Tipo de sangre:',choices=[('Pendiente','Pendiente'),('O+','O+'),('O-','O-'),('A+','A+'),('A-','A-'),('B+','B+'),('B-','B-'),('AB+','AB+'),('AB-','AB-')])
    seguro=SelectField('Aportación al seguro contra accidentes:',choices=[('',''),('Sí','Sí'),('No','No')])
    accion=RadioField('¿Qué deseas hacer?',choices=[('Modificar','Modificar expediente'),('Agregar','Nueva inscripción')])
    profesor_mod=SelectField('¿Cuál expediente deseas modificar?',choices=[('0','')])
    aviso_conf=RadioField('¿Firmó el aviso de confidencialidad?',choices=[('Sí','Sí'),('No','No')],validators=[InputRequired('Especifica si firmó el aviso de confidencialidad')])
    dia_nac=SelectField('Día de nacimiento:',choices=[('01','01'),('02','02'),('03','03'),('04','04'),('05','05'),('06','06'),('07','07'),('08','08'),('09','09'),('10','10'),('11','11'),('12','12'),('13','13'),('14','14'),('15','15'),('16','16'),('17','17'),('18','18'),('19','19'),('20','20'),('21','21'),('22','22'),('23','23'),('24','24'),('25','25'),('26','26'),('27','27'),('28','28'),('29','29'),('30','30'),('31','31')])
    mes_nac=SelectField('Mes de nacimiento:',choices=[('01','Enero'),('02','Febrero'),('03','Marzo'),('04','Abril'),('05','Mayo'),('06','Junio'),('07','Julio'),('08','Agosto'),('09','Septiembre'),('10','Octubre'),('11','Noviembre'),('12','Diciembre')])
    anio_nac=SelectField('Año de nacimiento:',choices=[('','')])
    menor=RadioField('Edad:',choices=[('Menor','Menor de edad'),('Mayor','Mayor de edad')], validators=[InputRequired('Indica si la persona es menor o mayor de edad')])


    def checar_email(self,email):
        if Profesores.query.filter_by(email=email).first():
            raise ValidationError('Este correo ya fue registrado anteriormente.')

    def checar_profesor(self,nombre):
        if Profesores.query.filter_by(nombre=nombre).first():
            raise ValidationError('Este profesor ya fue registrado anteriormente. No es necesario que lo vuelvas a hacer.')



class Agregar_clase_forma(FlaskForm):
    nombre=StringField('Nombre de la clase:')
    tipo=RadioField('',choices=[('Abierta','Abierta al público'),('Cerrada','Clase cerrada al público')],validators=[InputRequired('Indica si la clase está abierta al público o no')])
    descrip=TextAreaField('Descripción de la clase:')
    profesor_id=SelectField('Profesor:',coerce=int)
    realizar=SubmitField('Realizar')
    precio=TextAreaField('Precio:')
    horario=TextAreaField('Horario:')
    estatus=RadioField('Estatus de la clase',choices=[('Activa','Clase activa'),('Inactiva','Clase inactiva')],validators=[InputRequired('Indica si la clase aún está activa o no')])
    accion=RadioField('¿Qué deseas hacer?',choices=[('Modificar','Modificar una clase'),('Agregar','Agregar una clase nueva')])
    clase_mod=SelectField('¿Cuál clase deseas modificar?',choices=[('0','')])
    foto_clase=FileField('¿Tienes una foto  que ilustre la clase?', validators=[FileAllowed(['jpg','png','pdf'])])
    opinion=TextAreaField('Comentario del alumno:')
    clase_permanente=RadioField('Tipo de clase',choices=[('Permanente','Permanente'),('Temporal','Temporal')],validators=[InputRequired('Indica si la clase es permanente o temporal')])

class Comprar_clases_forma(FlaskForm):
    #alumno_id=IntegerField('Clave del alumno:')
    #clase_id=IntegerField('Clave de la clase:')
    n_clases=SelectField('Número de clases:',choices=[('','')])
    precio_tarjeta=FloatField('Precio del paquete de clases:')
    fec_vencimiento=StringField('Fecha de vencimiento del paquete de clases:',validators=[DataRequired('Indique la fecha de vencimiento')])
    comprar=SubmitField('Realizar compra')
    clase_id=SelectField('Clase:',coerce=int)
    alumno_id=SelectField('Alumno:',coerce=int)
    busca_alumno=StringField('Buscar alumno:')

    #opciones_clases=QuerySelectField(query_factory=consulta_clases, allow_blank=True)
class Pase_lista_forma(FlaskForm):
    clase_id=SelectField('Clase:',coerce=int)
    alumno_id=SelectField('Alumno:',coerce=int)
    busca_alumno=StringField('Buscar alumno:')
    registrar_asistencia=SubmitField('Registrar asistencia')

class Rentar_salon_forma(FlaskForm):
    salon=RadioField('Salón a rentar:',choices=[('Grande', 'Salón principal'),('Pequeño', 'Salón pequeño'),('Consultorio', 'Consultorio')],validators=[InputRequired('Indica el salón que se desea reservar')])
    precio_salon=StringField('Precio de la renta ($/hr.)')
    total_cant_pagar=SelectField('Cantidad a pagar por día:',choices=[('0','')])
    dia_renta=SelectMultipleField('Días:',choices=[('01','01'),('02','02'),('03','03'),('04','04'),('05','05'),('06','06'),('07','07'),('08','08'),('09','09'),('10','10'),('11','11'),('12','12'),('13','13'),('14','14'),('15','15'),('16','16'),('17','17'),('18','18'),('19','19'),('20','20'),('21','21'),('22','22'),('23','23'),('24','24'),('25','25'),('26','26'),('27','27'),('28','28'),('29','29'),('30','30'),('31','31')])
    mes_renta=SelectField('Mes:',choices=[('01','Enero'),('02','Febrero'),('03','Marzo'),('04','Abril'),('05','Mayo'),('06','Junio'),('07','Julio'),('08','Agosto'),('09','Septiembre'),('10','Octubre'),('11','Noviembre'),('12','Diciembre')])
    anio_renta=SelectField('Año:',choices=[('2018','2018'),('2019','2019'),('2020','2020'),('2021','2021'),('2022','2022'),('2023','2023'),('2024','2024'),('2025','2025'),('2026','2026'),('2027','2027'),('2028','2028')])
    #fec_renta=StringField('Fecha:')
    hora_inicial=StringField('Hora de inicio:')
    hora_final=StringField('Hora de finalización:')
    n_horas=SelectField('Horas reservadas por día:',choices=[('0','')])
    salon_cant_pagar=SelectField('Subtotal salón por día:',choices=[('0','')])
    serv_cant_pagar=SelectField('Subtotal servicios por día:',choices=[('0','')])
    profesor_id=SelectField('Profesor:',coerce=int)
    clase_id=SelectField('Clase:',coerce=int)
    servicios=SelectMultipleField('Servicios contratados:')
    accion=RadioField('¿Qué desea hacer?',choices=[('Cotizar','Cotizar renta'),('Rentar', 'Rentar salón'),('Cancelar', 'Cancelar renta')],validators=[InputRequired('Selecciona una opción')])
    #servicios=SelectMultipleField('Servicios contratados:', choices=[('1','Paquete básico'),('2','Paquete especial'),('3','Paquete Alto rendimiento')])

    rentar=SubmitField('Realizar')

class Agregar_servicio_forma(FlaskForm):
    nombre=StringField('Nombre:')
    descrip=TextAreaField('Descripción:')
    precio=FloatField('Precio:')
    agregar=SubmitField('Agregar nuevo servicio')

class Agregar_material_forma(FlaskForm):
    accion=RadioField('¿Qué deseas hacer?',choices=[('Modificar','Modificar inventario'),('Agregar', 'Agregar aparatos al inventario')])
    nombre=StringField('Material:')
    n_piezas=StringField('Número de piezas:')
    agregar=SubmitField('Realizar')
    fec_mantenimiento=StringField('Fecha de último mantenimiento:')
    estado=RadioField('Estado del material o aparato:',choices=[('Bien','Buen estado'),('Mal','Mal estado')],validators=[InputRequired('Indica el estado del material o de los aparatos')])
    observaciones=TextAreaField('Proporcione más información sobre el estado del material:')
    material_mod=SelectField('¿Cuál material deseas cambiar?',choices=[('','')])

    def checar_material(self,material):
        if Materiales.query.filter_by(nombre=material).first():
            raise ValidationError('Este material ya se encuentra registrado.')


class Armar_paquete_servicios_forma(FlaskForm):
    servicio_id=SelectField('Seleccionar servicio:',coerce=int)
    material_id=SelectMultipleField('Material disponible:')
    accion=RadioField('¿Qué desea hacer?',choices=[('Contenido','Mostrar contenido'),('Agregar', 'Agregar aparatos'),('Eliminar', 'Eliminar aparatos')])
    editar=SubmitField('Realizar')

class Cambiar_perfil_forma(FlaskForm):
    perfil=SelectField('Seleccionar grupo de usuarios',choices=[('Alumno','Alumnos'),('Personal','Personal administrativo'),('Directivo','Personal directivo')])
    usuario=SelectMultipleField('Usuarios:')
    accion=RadioField('¿Qué desea hacer?',choices=[('Contenido','Mostrar usuarios'),('Cambiar', 'Cambiar de grupo')])
    editar=SubmitField('Realizar')

class Agregar_concepto_forma(FlaskForm):
    accion=RadioField('¿Qué deseas hacer?',choices=[('Agregar','Agregar concepto'),('Eliminar','Eliminar concepto')])

    #concepto=StringField('Registra el concepto al que corresponde la operación:',validators=[DataRequired('Indica el concepto de la operación.')])
    concepto=StringField('Registra el concepto al que corresponde la operación:')
    realizar=SubmitField('Realizar')
    concepto_mod=SelectField('¿Cuál concepto desea eliminar?',choices=[('','')])

    def checar_concepto(self,concepto):
        if Conceptos.query.filter_by(concepto=concepto).first():
            raise ValidationError('Este concepto ya se encuentra registrado.')


class Agregar_movimientos_forma(FlaskForm):
    perfil=SelectField('¿Quién realiza el movimiento (i.e. Pago,retiro,etc.)?',choices=[('Alumno','Alumno'),('Profesor','Profesor'),('Profesor y alumno','Profesor y alumno'),('Administrativo','Equipo AEM'),('Proveedor','Proveedor')],validators=[InputRequired('Indica si deseas inscribir un alumno o profesor')])
    nombre=SelectField('Nombre:',choices=[('','')])
    mov=RadioField('Movimiento (Presione "Enter" depués de seleccionar una opción):',choices=[('Entrada','Entrada'),('Salida','Salida')],validators=[InputRequired('Indique si el movimiento es una entrada o salida de efectivo.')])
    monto=FloatField('Monto:',validators=[DataRequired('Debe especificar el monto de la operación.')])
    concepto=SelectField('Concepto del pago',choices=[('','')],validators=[DataRequired('Debe indicar el concepto de la operación.')])
    descrip=TextAreaField('Descripción:')
    realizar=SubmitField('Registrar')
    folio=IntegerField('Capture el folio del recibo:')

class Agregar_precio_clase_forma(FlaskForm):
    clase_id=SelectField('Clase:')
    precio=StringField('Cantidad a pagar por todas las clases:')
    cantidad=StringField('Indica el número de clases:')
    vigencia=StringField('Vigencia del paquete de clases:')
    accion=RadioField('¿Qué desea hacer?',choices=[('Contenido','Mostrar precios'),('Agregar', 'Agregar precio'),('Eliminar', 'Eliminar precio')])
    realizar=SubmitField('Realizar')

class Definir_reportes_forma(FlaskForm):
    reporte=StringField('Indica el nombre del reporte:',validators=[DataRequired('Indica el nombre del reporte')])
    tabla=StringField('Indica la tabla que deseas consultar:',validators=[DataRequired('Indica la tabla que deseas consultar')])
    encabezados=StringField('Indica los encabezados del reporte:',validators=[DataRequired('Indica los nombres de las variables')])
    realizar=SubmitField('Realizar')


class Reportes_forma(FlaskForm):
    reporte=SelectField('Selecciona el reporte:')
    #ruta=StringField('Indica la ruta en la que deseas descargar el archivo:')
    #nombre_archivo=StringField('Indica el nombre con el que deseas guardar el reporte')
    realizar=SubmitField('Generar reporte')

class Config_email_inst_forma(FlaskForm):
    email=StringField('Correo electrónico de la administración:',validators=[DataRequired('Indique la dirección de correo institucional'),Email('Cuenta incorrecta')])
    password=PasswordField('Contraseña',validators=[InputRequired('Indique la contraseña')])
    realizar=SubmitField('Agregar/Modificar')

    def checar_email(self):
        if Config_email_inst.query.first():
            return True
        else:
            return False

class Config_fondo_principal_forma(FlaskForm):
    fondo_principal=FileField('Indique la imagen del fondo principal del sitio:', validators=[FileAllowed(['jpg','png','pdf'])])
    actualizar=SubmitField('Actualizar')
